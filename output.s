.data
label_global:.space 4036
label_return:.space 40000
label_1:.asciiz "%d "
.align 2
label_2:.asciiz "%c"
.align 2
label_3:.asciiz "Sorry, the number n must be a number s.t. there exists i satisfying n=1+2+...+i\n"
.align 2
label_4:.asciiz "Let's start!\n"
.align 2
label_5:.asciiz "%d\n"
.align 2
label_6:.asciiz "step %d:\n"
.align 2
label_7:.asciiz "Total: %d step(s)\n"
.align 2
printf_buf: .space 2
.text
main:
la	$s0		label_global
la	$v1		label_return
li	$s1		210
sw	$s1		0($s0)
la	$s1		12($s0)
sw	$s1		4012($s0)
li	$s1		48271
sw	$s1		4016($s0)
li	$s1		2147483647
sw	$s1		4020($s0)
li	$s1		1
sw	$s1		4032($s0)
sw	$ra	0($sp)
add	$sp	$sp	-168
jal	label_main
lw	$ra	0($sp)
jr	$ra

label_random:
lw	$s1		4016($s0)
sw	$s1		4($sp)
lw	$s1		4032($s0)
sw	$s1		8($sp)
lw	$s1		8($sp)
lw	$s2		4024($s0)
rem	$s1	$s1	$s2
sw	$s1		8($sp)
lw	$s1		4($sp)
lw	$s2		8($sp)
mul	$s1	$s1	$s2
sw	$s1		4($sp)
lw	$s1		4($sp)
sw	$s1		12($sp)
lw	$s1		4028($s0)
sw	$s1		16($sp)
lw	$s1		4032($s0)
sw	$s1		20($sp)
lw	$s1		20($sp)
lw	$s2		4024($s0)
div	$s1	$s1	$s2
sw	$s1		20($sp)
lw	$s1		16($sp)
lw	$s2		20($sp)
mul	$s1	$s1	$s2
sw	$s1		16($sp)
lw	$s1		12($sp)
lw	$s2		16($sp)
sub	$s1	$s1	$s2
sw	$s1		12($sp)
lw	$s1		12($sp)
sw	$s1		0($sp)
lw	$s1		0($sp)
sw	$s1		24($sp)
lw	$s1		24($sp)
sge	$s1	$s1	0
sw	$s1		24($sp)
lw	$s1		24($sp)
beqz	$s1		L0
lw	$s1		0($sp)
sw	$s1		4032($s0)
b	L1
L0:
lw	$s1		0($sp)
sw	$s1		28($sp)
lw	$s1		28($sp)
lw	$s2		4020($s0)
add	$s1	$s1	$s2
sw	$s1		28($sp)
lw	$s1		28($sp)
sw	$s1		4032($s0)
L1:
lw	$s1		4032($s0)
sw	$s1		0($v1)
add	$sp	$sp	36
jr	$ra



label_initialize:
lw	$s1		0($sp)
sw	$s1		4032($s0)
add	$sp	$sp	8
jr	$ra



label_swap:
li  $s1     0
lw  $s1     0($sp)
lw	$s1		0($sp)
mul	$s1	$s1	4
lw	$s2		4012($s0)
add	$s2	$s1	$s2
sw	$s2		12($sp)
lw	$s1		12($sp)
lw	$s1	0($s1)
sw	$s1		8($sp)
lw	$s1		0($sp)
mul	$s1	$s1	4
lw	$s2		4012($s0)
add	$s2	$s1	$s2
sw	$s2		16($sp)
lw	$s1		4($sp)
mul	$s1	$s1	4
lw	$s2		4012($s0)
add	$s2	$s1	$s2
sw	$s2		20($sp)
lw	$s1		20($sp)
lw	$s1	0($s1)
lw	$s2		16($sp)
sw	$s1	0($s2)
lw	$s1		4($sp)
mul	$s1	$s1	4
lw	$s2		4012($s0)
add	$s2	$s1	$s2
sw	$s2		24($sp)
lw	$s1		8($sp)
lw	$s2		24($sp)
sw	$s1	0($s2)
add	$sp	$sp	32
jr	$ra



label_pd:
L3:
lw	$s1		4($s0)
sw	$s1		4($sp)
lw	$s1		4($sp)
lw	$s2		0($sp)
sle	$s1	$s1	$s2
sw	$s1		4($sp)
lw	$s1		4($sp)
beqz	$s1		L2
lw	$s1		0($sp)
sw	$s1		8($sp)
lw	$s1		4($s0)
sw	$s1		12($sp)
lw	$s1		4($s0)
sw	$s1		16($sp)
lw	$s1		16($sp)
add	$s1	$s1	1
sw	$s1		16($sp)
lw	$s1		12($sp)
lw	$s2		16($sp)
mul	$s1	$s1	$s2
sw	$s1		12($sp)
lw	$s1		12($sp)
div	$s1	$s1	2
sw	$s1		12($sp)
lw	$s1		8($sp)
lw	$s2		12($sp)
seq	$s1	$s1	$s2
sw	$s1		8($sp)
lw	$s1		8($sp)
beqz	$s1		L4
li	$s1		1
sw	$s1		0($v1)
add	$sp	$sp	24
jr	$ra
L4:
L5:
lw	$s1		4($s0)
add	$s1	$s1	1
sw	$s1		4($s0)
b	L3
L2:
li	$s1		0
sw	$s1		0($v1)
add	$sp	$sp	24
jr	$ra



label_show:
li	$s1		0
sw	$s1		0($sp)
L7:
lw	$s1		0($sp)
sw	$s1		4($sp)
lw	$s1		4($sp)
lw	$s2		8($s0)
slt	$s1	$s1	$s2
sw	$s1		4($sp)
lw	$s1		4($sp)
beqz	$s1		L6
la	$a0	label_1
lw	$s1		0($sp)
mul	$s1	$s1	4
lw	$s2		4012($s0)
add	$s2	$s1	$s2
sw	$s2		8($sp)
lw	$s1		8($sp)
lw	$a1	0($s1)
sw	$ra	12($sp)
jal	printf
lw	$ra	12($sp)
L8:
lw	$s1		0($sp)
add	$s1	$s1	1
sw	$s1		0($sp)
b	L7
L6:
la	$a0	label_2
li	$a1	10
sw	$ra	16($sp)
jal	printf
lw	$ra	16($sp)
add	$sp	$sp	24
jr	$ra



label_win:
la	$s1		8($sp)
sw	$s1		4008($sp)
li	$s1		0
sw	$s1		4($sp)
lw	$s1		8($s0)
sw	$s1		4016($sp)
lw	$s1		4016($sp)
lw	$s2		4($s0)
sne	$s1	$s1	$s2
sw	$s1		4016($sp)
lw	$s1		4016($sp)
beqz	$s1		L9
li	$s1		0
sw	$s1		0($v1)
add	$sp	$sp	4096
jr	$ra
L9:
L11:
lw	$s1		4($sp)
sw	$s1		4020($sp)
lw	$s1		4020($sp)
lw	$s2		8($s0)
slt	$s1	$s1	$s2
sw	$s1		4020($sp)
lw	$s1		4020($sp)
beqz	$s1		L10
lw	$s1		4($sp)
mul	$s1	$s1	4
lw	$s2		4008($sp)
add	$s2	$s1	$s2
sw	$s2		4024($sp)
lw	$s1		4($sp)
mul	$s1	$s1	4
lw	$s2		4012($s0)
add	$s2	$s1	$s2
sw	$s2		4028($sp)
lw	$s1		4028($sp)
lw	$s1	0($s1)
lw	$s2		4024($sp)
sw	$s1	0($s2)
L12:
lw	$s1		4($sp)
add	$s1	$s1	1
sw	$s1		4($sp)
b	L11
L10:
li	$s1		0
sw	$s1		0($sp)
L14:
lw	$s1		0($sp)
sw	$s1		4032($sp)
lw	$s1		8($s0)
sw	$s1		4036($sp)
lw	$s1		4036($sp)
sub	$s1	$s1	1
sw	$s1		4036($sp)
lw	$s1		4032($sp)
lw	$s2		4036($sp)
slt	$s1	$s1	$s2
sw	$s1		4032($sp)
lw	$s1		4032($sp)
beqz	$s1		L13
lw	$s1		0($sp)
sw	$s1		4040($sp)
lw	$s1		4040($sp)
add	$s1	$s1	1
sw	$s1		4040($sp)
lw	$s1		4040($sp)
sw	$s1		4($sp)
L16:
lw	$s1		4($sp)
sw	$s1		4044($sp)
lw	$s1		4044($sp)
lw	$s2		8($s0)
slt	$s1	$s1	$s2
sw	$s1		4044($sp)
lw	$s1		4044($sp)
beqz	$s1		L15
lw	$s1		0($sp)
mul	$s1	$s1	4
lw	$s2		4008($sp)
add	$s2	$s1	$s2
sw	$s2		4048($sp)
lw	$s1		4048($sp)
lw	$s1	0($s1)
sw	$s1		4052($sp)
lw	$s1		4($sp)
mul	$s1	$s1	4
lw	$s2		4008($sp)
add	$s2	$s1	$s2
sw	$s2		4056($sp)
lw	$s1		4052($sp)
lw	$s2		4056($sp)
lw	$s2	0($s2)
sgt	$s1	$s1	$s2
sw	$s1		4052($sp)
lw	$s1		4052($sp)
beqz	$s1		L17
lw	$s1		0($sp)
mul	$s1	$s1	4
lw	$s2		4008($sp)
add	$s2	$s1	$s2
sw	$s2		4060($sp)
lw	$s1		4060($sp)
lw	$s2	0($s1)
sw	$s2		4012($sp)
lw	$s1		0($sp)
mul	$s1	$s1	4
lw	$s2		4008($sp)
add	$s2	$s1	$s2
sw	$s2		4064($sp)
lw	$s1		4($sp)
mul	$s1	$s1	4
lw	$s2		4008($sp)
add	$s2	$s1	$s2
sw	$s2		4068($sp)
lw	$s1		4068($sp)
lw	$s1	0($s1)
lw	$s2		4064($sp)
sw	$s1	0($s2)
lw	$s1		4($sp)
mul	$s1	$s1	4
lw	$s2		4008($sp)
add	$s2	$s1	$s2
sw	$s2		4072($sp)
lw	$s1		4012($sp)
lw	$s2		4072($sp)
sw	$s1	0($s2)
L17:
L18:
lw	$s1		4($sp)
add	$s1	$s1	1
sw	$s1		4($sp)
b	L16
L15:
L19:
lw	$s1		0($sp)
add	$s1	$s1	1
sw	$s1		0($sp)
b	L14
L13:
li	$s1		0
sw	$s1		0($sp)
L21:
lw	$s1		0($sp)
sw	$s1		4076($sp)
lw	$s1		4076($sp)
lw	$s2		8($s0)
slt	$s1	$s1	$s2
sw	$s1		4076($sp)
lw	$s1		4076($sp)
beqz	$s1		L20
lw	$s1		0($sp)
mul	$s1	$s1	4
lw	$s2		4008($sp)
add	$s2	$s1	$s2
sw	$s2		4080($sp)
lw	$s1		4080($sp)
lw	$s1	0($s1)
sw	$s1		4084($sp)
lw	$s1		0($sp)
sw	$s1		4088($sp)
lw	$s1		4088($sp)
add	$s1	$s1	1
sw	$s1		4088($sp)
lw	$s1		4084($sp)
lw	$s2		4088($sp)
sne	$s1	$s1	$s2
sw	$s1		4084($sp)
lw	$s1		4084($sp)
beqz	$s1		L22
li	$s1		0
sw	$s1		0($v1)
add	$sp	$sp	4096
jr	$ra
L22:
L23:
lw	$s1		0($sp)
add	$s1	$s1	1
sw	$s1		0($sp)
b	L21
L20:
li	$s1		1
sw	$s1		0($v1)
add	$sp	$sp	4096
jr	$ra



label_merge:
li	$s1		0
sw	$s1		0($sp)
L25:
lw	$s1		0($sp)
sw	$s1		4($sp)
lw	$s1		4($sp)
lw	$s2		8($s0)
slt	$s1	$s1	$s2
sw	$s1		4($sp)
lw	$s1		4($sp)
beqz	$s1		L24
lw	$s1		0($sp)
mul	$s1	$s1	4
lw	$s2		4012($s0)
add	$s2	$s1	$s2
sw	$s2		8($sp)
lw	$s1		8($sp)
lw	$s1	0($s1)
seq	$s1	$s1	0
sw	$s1		12($sp)
lw	$s1		12($sp)
beqz	$s1		L30
lw	$s1		0($sp)
sw	$s1		20($sp)
lw	$s1		20($sp)
add	$s1	$s1	1
sw	$s1		20($sp)
lw	$s1		20($sp)
sw	$s1		16($sp)
L27:
lw	$s1		16($sp)
sw	$s1		24($sp)
lw	$s1		24($sp)
lw	$s2		8($s0)
slt	$s1	$s1	$s2
sw	$s1		24($sp)
lw	$s1		24($sp)
beqz	$s1		L26
lw	$s1		16($sp)
mul	$s1	$s1	4
lw	$s2		4012($s0)
add	$s2	$s1	$s2
sw	$s2		28($sp)
lw	$s1		28($sp)
lw	$s1	0($s1)
sw	$s1		32($sp)
lw	$s1		32($sp)
sne	$s1	$s1	0
sw	$s1		32($sp)
lw	$s1		32($sp)
beqz	$s1		L28
sw	$ra	36($sp)
add	$sp	$sp	-32
lw	$s1	32($sp)
sw	$s1		0($sp)
lw	$s1	48($sp)
sw	$s1		4($sp)
jal	label_swap
lw	$ra	36($sp)
lw	$s1		0($v1)
sw	$s1		40($sp)
b	L26
L28:
L29:
lw	$s1		16($sp)
add	$s1	$s1	1
sw	$s1		16($sp)
b	L27
L26:
L30:
L31:
lw	$s1		0($sp)
add	$s1	$s1	1
sw	$s1		0($sp)
b	L25
L24:
li	$s1		0
sw	$s1		0($sp)
L33:
lw	$s1		0($sp)
sw	$s1		40($sp)
lw	$s1		40($sp)
lw	$s2		8($s0)
slt	$s1	$s1	$s2
sw	$s1		40($sp)
lw	$s1		40($sp)
beqz	$s1		L32
lw	$s1		0($sp)
mul	$s1	$s1	4
lw	$s2		4012($s0)
add	$s2	$s1	$s2
sw	$s2		44($sp)
lw	$s1		44($sp)
lw	$s1	0($s1)
seq	$s1	$s1	0
sw	$s1		48($sp)
lw	$s1		48($sp)
beqz	$s1		L34
lw	$s1		0($sp)
sw	$s1		8($s0)
b	L32
L34:
L35:
lw	$s1		0($sp)
add	$s1	$s1	1
sw	$s1		0($sp)
b	L33
L32:
add	$sp	$sp	56
jr	$ra



label_move:
li	$s1		0
sw	$s1		0($sp)
L37:
lw	$s1		0($sp)
sw	$s1		4($sp)
lw	$s1		4($sp)
lw	$s2		8($s0)
slt	$s1	$s1	$s2
sw	$s1		4($sp)
lw	$s1		4($sp)
beqz	$s1		L36
L38:
lw	$s1		0($sp)
mul	$s1	$s1	4
lw	$s2		4012($s0)
add	$s2	$s1	$s2
sw	$s2		8($sp)
lw	$s1		8($sp)
lw	$s2	0($s1)
sub	$s2	$s2	1
sw	$s2	0($s1)
lw	$s1		0($sp)
sw	$s1		12($sp)
lw	$s1		12($sp)
add	$s1	$s1	1
sw	$s1		12($sp)
lw	$s1		12($sp)
sw	$s1		0($sp)
b	L37
L36:
lw	$s1		8($s0)
mul	$s1	$s1	4
lw	$s2		4012($s0)
add	$s2	$s1	$s2
sw	$s2		16($sp)
lw	$s1		8($s0)
lw	$s2		16($sp)
sw	$s1	0($s2)
lw	$s1		8($s0)
sw	$s1		20($sp)
add	$s1	$s1	1
sw	$s1		8($s0)
add	$sp	$sp	28
jr	$ra



label_main:
li	$s1		0
sw	$s1		0($sp)
li	$s1		0
sw	$s1		4($sp)
li	$s1		0
sw	$s1		8($sp)
lw	$s1		4020($s0)
sw	$s1		12($sp)
lw	$s1		12($sp)
lw	$s2		4016($s0)
div	$s1	$s1	$s2
sw	$s1		12($sp)
lw	$s1		12($sp)
sw	$s1		4024($s0)
lw	$s1		4020($s0)
sw	$s1		16($sp)
lw	$s1		16($sp)
lw	$s2		4016($s0)
rem	$s1	$s1	$s2
sw	$s1		16($sp)
lw	$s1		16($sp)
sw	$s1		4028($s0)
sw	$ra	20($sp)
add	$sp	$sp	-24
lw	$s1	0($s0)
sw	$s1		0($sp)
jal	label_pd
lw	$ra	20($sp)
lw	$s1		0($v1)
sw	$s1		24($sp)
lw	$s1		24($sp)
seq	$s1	$s1	0
sw	$s1		28($sp)
lw	$s1		28($sp)
beqz	$s1		L39
la	$a0	label_3
sw	$ra	32($sp)
jal	printf
lw	$ra	32($sp)
li	$s1		1
sw	$s1		0($v1)
add	$sp	$sp	168
jr	$ra
L39:
la	$a0	label_4
sw	$ra	36($sp)
jal	printf
lw	$ra	36($sp)
sw	$ra	40($sp)
add	$sp	$sp	-8
li	$s1		3654898
sw	$s1		0($sp)
jal	label_initialize
lw	$ra	40($sp)
lw	$s1		0($v1)
sw	$s1		44($sp)
sw	$ra	44($sp)
add	$sp	$sp	-36
jal	label_random
lw	$ra	44($sp)
lw	$s1		0($v1)
sw	$s1		48($sp)
lw	$s1		48($sp)
rem	$s1	$s1	10
sw	$s1		48($sp)
lw	$s1		48($sp)
sw	$s1		52($sp)
lw	$s1		52($sp)
add	$s1	$s1	1
sw	$s1		52($sp)
lw	$s1		52($sp)
sw	$s1		8($s0)
la	$a0	label_5
lw	$a1	8($s0)
sw	$ra	56($sp)
jal	printf
lw	$ra	56($sp)
L41:
lw	$s1		0($sp)
sw	$s1		60($sp)
lw	$s1		8($s0)
sw	$s1		64($sp)
lw	$s1		64($sp)
sub	$s1	$s1	1
sw	$s1		64($sp)
lw	$s1		60($sp)
lw	$s2		64($sp)
slt	$s1	$s1	$s2
sw	$s1		60($sp)
lw	$s1		60($sp)
beqz	$s1		L40
lw	$s1		0($sp)
mul	$s1	$s1	4
lw	$s2		4012($s0)
add	$s2	$s1	$s2
sw	$s2		68($sp)
sw	$ra	72($sp)
add	$sp	$sp	-36
jal	label_random
lw	$ra	72($sp)
lw	$s1		0($v1)
sw	$s1		76($sp)
lw	$s1		76($sp)
rem	$s1	$s1	10
sw	$s1		76($sp)
lw	$s1		76($sp)
sw	$s1		80($sp)
lw	$s1		80($sp)
add	$s1	$s1	1
sw	$s1		80($sp)
lw	$s1		80($sp)
lw	$s2		68($sp)
sw	$s1	0($s2)
L43:
lw	$s1		0($sp)
mul	$s1	$s1	4
lw	$s2		4012($s0)
add	$s2	$s1	$s2
sw	$s2		84($sp)
lw	$s1		84($sp)
lw	$s1	0($s1)
sw	$s1		88($sp)
lw	$s1		88($sp)
lw	$s2		4($sp)
add	$s1	$s1	$s2
sw	$s1		88($sp)
lw	$s1		88($sp)
sw	$s1		92($sp)
lw	$s1		92($sp)
lw	$s2		0($s0)
sgt	$s1	$s1	$s2
sw	$s1		92($sp)
lw	$s1		92($sp)
beqz	$s1		L42
lw	$s1		0($sp)
mul	$s1	$s1	4
lw	$s2		4012($s0)
add	$s2	$s1	$s2
sw	$s2		96($sp)
sw	$ra	100($sp)
add	$sp	$sp	-36
jal	label_random
lw	$ra	100($sp)
lw	$s1		0($v1)
sw	$s1		104($sp)
lw	$s1		104($sp)
rem	$s1	$s1	10
sw	$s1		104($sp)
lw	$s1		104($sp)
sw	$s1		108($sp)
lw	$s1		108($sp)
add	$s1	$s1	1
sw	$s1		108($sp)
lw	$s1		108($sp)
lw	$s2		96($sp)
sw	$s1	0($s2)
b	L43
L42:
lw	$s1		0($sp)
mul	$s1	$s1	4
lw	$s2		4012($s0)
add	$s2	$s1	$s2
sw	$s2		112($sp)
lw	$s1		4($sp)
lw	$s2		112($sp)
lw	$s3	0($s2)
add	$s1	$s1	$s3
sw	$s1		4($sp)
L44:
lw	$s1		0($sp)
add	$s1	$s1	1
sw	$s1		0($sp)
b	L41
L40:
lw	$s1		8($s0)
sw	$s1		116($sp)
lw	$s1		116($sp)
sub	$s1	$s1	1
sw	$s1		116($sp)
lw	$s1		116($sp)
mul	$s1	$s1	4
lw	$s2		4012($s0)
add	$s2	$s1	$s2
sw	$s2		120($sp)
lw	$s1		0($s0)
sw	$s1		124($sp)
lw	$s1		124($sp)
lw	$s2		4($sp)
sub	$s1	$s1	$s2
sw	$s1		124($sp)
lw	$s1		124($sp)
lw	$s2		120($sp)
sw	$s1	0($s2)
sw	$ra	128($sp)
add	$sp	$sp	-24
jal	label_show
lw	$ra	128($sp)
sw	$ra	132($sp)
add	$sp	$sp	-56
jal	label_merge
lw	$ra	132($sp)
L46:
sw	$ra	136($sp)
add	$sp	$sp	-4096
jal	label_win
lw	$ra	136($sp)
lw	$s1		0($v1)
seq	$s1	$s1	0
sw	$s1		140($sp)
lw	$s1		140($sp)
beqz	$s1		L45
la	$a0	label_6
lw	$s1		8($sp)
add	$s1	$s1	1
sw	$s1		8($sp)
lw	$a1	8($sp)
sw	$ra	144($sp)
jal	printf
lw	$ra	144($sp)
sw	$ra	148($sp)
add	$sp	$sp	-28
jal	label_move
lw	$ra	148($sp)
sw	$ra	152($sp)
add	$sp	$sp	-56
jal	label_merge
lw	$ra	152($sp)
sw	$ra	156($sp)
add	$sp	$sp	-24
jal	label_show
lw	$ra	156($sp)
b	L46
L45:
la	$a0	label_7
lw	$a1	8($sp)
sw	$ra	160($sp)
jal	printf
lw	$ra	160($sp)
li	$s1		0
sw	$s1		0($v1)
add	$sp	$sp	168
jr	$ra


tranchar:
	
	
	add $a0, $a0, $a1
	lb $v0, 0($a0)
	jr $ra

printf2:
	
	li $v0, 4
	syscall
	jr $ra

malloc:
	
	li $v0, 9
	syscall
	jr $ra

initArray:
	
	
	li $v0, 9
	syscall
	move $a3, $v0
	add $a0, $a0, $v0
_initArray_loop:
	sw $a1, 0($a3)
	add $a3, $a3, 4
	bne $a3, $a0, _initArray_loop
	jr $ra

printf:
	subu $sp, $sp, 44 
	sw $ra, 32($sp) 
	sw $fp, 28($sp)
	sw $s0, 24($sp)
	sw $s1, 20($sp)
	sw $s2, 16($sp)
	sw $s3, 12($sp)
	sw $s4, 8($sp)
	sw $s5, 4($sp)
	sw $s6, 0($sp)
	sw $s7, 36($sp)
	sw $s8, 40($sp)
	addu $fp, $sp, 36


	move $s0, $a0 
	move $s1, $a1 
	move $s2, $a2 
	move $s3, $a3 
	lw $s7, 16($v1)
	lw $s8, 20($v1)

	li $s4, 0 
	la $s6, printf_buf 

printf_loop: 
	lb $s5, 0($s0) 
	addu $s0, $s0, 1 

	beq $s5, '%', printf_fmt 
	beq $0, $s5, printf_end 

printf_putc:
	sb $s5, 0($s6) 
	sb $0, 1($s6) 
	move $a0, $s6 
	li $v0, 4 
	syscall

	b printf_loop 

printf_fmt:
	lb $s5, 0($s0) 
	addu $s0, $s0, 1 

	beq $s5, '0', printf_prefix
	beq $s5, 'd', printf_int 
	beq $s5, 's', printf_str 
	beq $s5, 'c', printf_char 
	beq $s5, '%', printf_perc 
	b printf_loop 

printf_shift_args: 
	move $s1, $s2 
	move $s2, $s3 
	move $s3, $s7 
	move $s7, $s8 

	add $s4, $s4, 1 

	b printf_loop 

printf_int: 
	move $a0, $s1 
	li $v0, 1
	syscall
	b printf_shift_args 

printf_str: 
	move $a0, $s1 
	li $v0, 4
	syscall
	b printf_shift_args 

printf_char: 
	sb $s1, 0($s6) 
	sb $0, 1($s6) 
	move $a0, $s6 
	li $v0, 4 
	syscall
	b printf_shift_args 

printf_perc: 
	li $s5, '%' 
	sb $s5, 0($s6) 
	sb $0, 1($s6) 
	move $a0, $s6 
	li $v0, 4 
	syscall
	b printf_loop 

printf_prefix: 
	lb $s5, 0($s0)
	add $s0, $s0, 1
	li $s7, 1
	printf_prefix_loop_1:
	mul $s7, $s7, 10
	sub $s5, $s5, 1
	bgt $s5, '1', printf_prefix_loop_1
	printf_prefix_loop_2:
	move $a0, $s1
	div $a0, $a0, $s7
	rem $a0, $a0, 10
	li $v0, 1
	syscall
	div $s7, $s7, 10
	bge $s7, 1, printf_prefix_loop_2
	lb $s5, 0($s0)
	addu $s0, $s0, 1
	b printf_shift_args 

printf_end:
	lw $s8, 40($sp)
	lw $s7, 36($sp)
	lw $ra, 32($sp) 
	lw $fp, 28($sp)
	lw $s0, 24($sp)
	lw $s1, 20($sp)
	lw $s2, 16($sp)
	lw $s3, 12($sp)
	lw $s4, 8($sp)
	lw $s5, 4($sp)
	lw $s6, 0($sp)
	addu $sp, $sp, 44 
	jr $ra 










// Generated from C2014.g4 by ANTLR 4.2

	package parser;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link C2014Parser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface C2014Visitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link C2014Parser#compound_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCompound_statement(@NotNull C2014Parser.Compound_statementContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#cast_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCast_expression(@NotNull C2014Parser.Cast_expressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#type_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_name(@NotNull C2014Parser.Type_nameContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#additive_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdditive_operator(@NotNull C2014Parser.Additive_operatorContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant(@NotNull C2014Parser.ConstantContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#init_declarator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInit_declarator(@NotNull C2014Parser.Init_declaratorContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#postfix_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfix_expression(@NotNull C2014Parser.Postfix_expressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#program}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProgram(@NotNull C2014Parser.ProgramContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#equality_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEquality_operator(@NotNull C2014Parser.Equality_operatorContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#unary_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_expression(@NotNull C2014Parser.Unary_expressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#logical_or_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogical_or_expression(@NotNull C2014Parser.Logical_or_expressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#iteration_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIteration_statement(@NotNull C2014Parser.Iteration_statementContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#and_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAnd_expression(@NotNull C2014Parser.And_expressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#additive_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdditive_expression(@NotNull C2014Parser.Additive_expressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#assignment_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment_operator(@NotNull C2014Parser.Assignment_operatorContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#multiplicative_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicative_expression(@NotNull C2014Parser.Multiplicative_expressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#primary_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimary_expression(@NotNull C2014Parser.Primary_expressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#identifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifier(@NotNull C2014Parser.IdentifierContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#unary_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnary_operator(@NotNull C2014Parser.Unary_operatorContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#relational_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelational_operator(@NotNull C2014Parser.Relational_operatorContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#exclusive_or_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExclusive_or_expression(@NotNull C2014Parser.Exclusive_or_expressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#multiplicative_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicative_operator(@NotNull C2014Parser.Multiplicative_operatorContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#parameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameters(@NotNull C2014Parser.ParametersContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#struct_or_union}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStruct_or_union(@NotNull C2014Parser.Struct_or_unionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#typedef_name}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTypedef_name(@NotNull C2014Parser.Typedef_nameContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#equality_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEquality_expression(@NotNull C2014Parser.Equality_expressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#plain_declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlain_declaration(@NotNull C2014Parser.Plain_declarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#string}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitString(@NotNull C2014Parser.StringContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#init_declarators}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInit_declarators(@NotNull C2014Parser.Init_declaratorsContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#integer_constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInteger_constant(@NotNull C2014Parser.Integer_constantContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#inclusive_or_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInclusive_or_expression(@NotNull C2014Parser.Inclusive_or_expressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#selection_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitSelection_statement(@NotNull C2014Parser.Selection_statementContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#constant_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConstant_expression(@NotNull C2014Parser.Constant_expressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStatement(@NotNull C2014Parser.StatementContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#shift_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShift_expression(@NotNull C2014Parser.Shift_expressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#postfix}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostfix(@NotNull C2014Parser.PostfixContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression(@NotNull C2014Parser.ExpressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#type_specifier}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitType_specifier(@NotNull C2014Parser.Type_specifierContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#expression_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExpression_statement(@NotNull C2014Parser.Expression_statementContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#assignment_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAssignment_expression(@NotNull C2014Parser.Assignment_expressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#character_constant}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharacter_constant(@NotNull C2014Parser.Character_constantContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#declaration}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclaration(@NotNull C2014Parser.DeclarationContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#declarator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclarator(@NotNull C2014Parser.DeclaratorContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#initializer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInitializer(@NotNull C2014Parser.InitializerContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#shift_operator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShift_operator(@NotNull C2014Parser.Shift_operatorContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#declarators}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDeclarators(@NotNull C2014Parser.DeclaratorsContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#logical_and_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLogical_and_expression(@NotNull C2014Parser.Logical_and_expressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#plain_declarator}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPlain_declarator(@NotNull C2014Parser.Plain_declaratorContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#arguments}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArguments(@NotNull C2014Parser.ArgumentsContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#relational_expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelational_expression(@NotNull C2014Parser.Relational_expressionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#function_definition}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction_definition(@NotNull C2014Parser.Function_definitionContext ctx);

	/**
	 * Visit a parse tree produced by {@link C2014Parser#jump_statement}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitJump_statement(@NotNull C2014Parser.Jump_statementContext ctx);
}
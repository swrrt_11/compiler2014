// Generated from C2014.g4 by ANTLR 4.2

	package parser;

import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link C2014Parser}.
 */
public interface C2014Listener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link C2014Parser#compound_statement}.
	 * @param ctx the parse tree
	 */
	void enterCompound_statement(@NotNull C2014Parser.Compound_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#compound_statement}.
	 * @param ctx the parse tree
	 */
	void exitCompound_statement(@NotNull C2014Parser.Compound_statementContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#cast_expression}.
	 * @param ctx the parse tree
	 */
	void enterCast_expression(@NotNull C2014Parser.Cast_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#cast_expression}.
	 * @param ctx the parse tree
	 */
	void exitCast_expression(@NotNull C2014Parser.Cast_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#type_name}.
	 * @param ctx the parse tree
	 */
	void enterType_name(@NotNull C2014Parser.Type_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#type_name}.
	 * @param ctx the parse tree
	 */
	void exitType_name(@NotNull C2014Parser.Type_nameContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#additive_operator}.
	 * @param ctx the parse tree
	 */
	void enterAdditive_operator(@NotNull C2014Parser.Additive_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#additive_operator}.
	 * @param ctx the parse tree
	 */
	void exitAdditive_operator(@NotNull C2014Parser.Additive_operatorContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#constant}.
	 * @param ctx the parse tree
	 */
	void enterConstant(@NotNull C2014Parser.ConstantContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#constant}.
	 * @param ctx the parse tree
	 */
	void exitConstant(@NotNull C2014Parser.ConstantContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#init_declarator}.
	 * @param ctx the parse tree
	 */
	void enterInit_declarator(@NotNull C2014Parser.Init_declaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#init_declarator}.
	 * @param ctx the parse tree
	 */
	void exitInit_declarator(@NotNull C2014Parser.Init_declaratorContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#postfix_expression}.
	 * @param ctx the parse tree
	 */
	void enterPostfix_expression(@NotNull C2014Parser.Postfix_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#postfix_expression}.
	 * @param ctx the parse tree
	 */
	void exitPostfix_expression(@NotNull C2014Parser.Postfix_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#program}.
	 * @param ctx the parse tree
	 */
	void enterProgram(@NotNull C2014Parser.ProgramContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#program}.
	 * @param ctx the parse tree
	 */
	void exitProgram(@NotNull C2014Parser.ProgramContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#equality_operator}.
	 * @param ctx the parse tree
	 */
	void enterEquality_operator(@NotNull C2014Parser.Equality_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#equality_operator}.
	 * @param ctx the parse tree
	 */
	void exitEquality_operator(@NotNull C2014Parser.Equality_operatorContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#unary_expression}.
	 * @param ctx the parse tree
	 */
	void enterUnary_expression(@NotNull C2014Parser.Unary_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#unary_expression}.
	 * @param ctx the parse tree
	 */
	void exitUnary_expression(@NotNull C2014Parser.Unary_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#logical_or_expression}.
	 * @param ctx the parse tree
	 */
	void enterLogical_or_expression(@NotNull C2014Parser.Logical_or_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#logical_or_expression}.
	 * @param ctx the parse tree
	 */
	void exitLogical_or_expression(@NotNull C2014Parser.Logical_or_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#iteration_statement}.
	 * @param ctx the parse tree
	 */
	void enterIteration_statement(@NotNull C2014Parser.Iteration_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#iteration_statement}.
	 * @param ctx the parse tree
	 */
	void exitIteration_statement(@NotNull C2014Parser.Iteration_statementContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#and_expression}.
	 * @param ctx the parse tree
	 */
	void enterAnd_expression(@NotNull C2014Parser.And_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#and_expression}.
	 * @param ctx the parse tree
	 */
	void exitAnd_expression(@NotNull C2014Parser.And_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#additive_expression}.
	 * @param ctx the parse tree
	 */
	void enterAdditive_expression(@NotNull C2014Parser.Additive_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#additive_expression}.
	 * @param ctx the parse tree
	 */
	void exitAdditive_expression(@NotNull C2014Parser.Additive_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#assignment_operator}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_operator(@NotNull C2014Parser.Assignment_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#assignment_operator}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_operator(@NotNull C2014Parser.Assignment_operatorContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#multiplicative_expression}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicative_expression(@NotNull C2014Parser.Multiplicative_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#multiplicative_expression}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicative_expression(@NotNull C2014Parser.Multiplicative_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#primary_expression}.
	 * @param ctx the parse tree
	 */
	void enterPrimary_expression(@NotNull C2014Parser.Primary_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#primary_expression}.
	 * @param ctx the parse tree
	 */
	void exitPrimary_expression(@NotNull C2014Parser.Primary_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#identifier}.
	 * @param ctx the parse tree
	 */
	void enterIdentifier(@NotNull C2014Parser.IdentifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#identifier}.
	 * @param ctx the parse tree
	 */
	void exitIdentifier(@NotNull C2014Parser.IdentifierContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void enterUnary_operator(@NotNull C2014Parser.Unary_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#unary_operator}.
	 * @param ctx the parse tree
	 */
	void exitUnary_operator(@NotNull C2014Parser.Unary_operatorContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#relational_operator}.
	 * @param ctx the parse tree
	 */
	void enterRelational_operator(@NotNull C2014Parser.Relational_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#relational_operator}.
	 * @param ctx the parse tree
	 */
	void exitRelational_operator(@NotNull C2014Parser.Relational_operatorContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#exclusive_or_expression}.
	 * @param ctx the parse tree
	 */
	void enterExclusive_or_expression(@NotNull C2014Parser.Exclusive_or_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#exclusive_or_expression}.
	 * @param ctx the parse tree
	 */
	void exitExclusive_or_expression(@NotNull C2014Parser.Exclusive_or_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#multiplicative_operator}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicative_operator(@NotNull C2014Parser.Multiplicative_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#multiplicative_operator}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicative_operator(@NotNull C2014Parser.Multiplicative_operatorContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#parameters}.
	 * @param ctx the parse tree
	 */
	void enterParameters(@NotNull C2014Parser.ParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#parameters}.
	 * @param ctx the parse tree
	 */
	void exitParameters(@NotNull C2014Parser.ParametersContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#struct_or_union}.
	 * @param ctx the parse tree
	 */
	void enterStruct_or_union(@NotNull C2014Parser.Struct_or_unionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#struct_or_union}.
	 * @param ctx the parse tree
	 */
	void exitStruct_or_union(@NotNull C2014Parser.Struct_or_unionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#typedef_name}.
	 * @param ctx the parse tree
	 */
	void enterTypedef_name(@NotNull C2014Parser.Typedef_nameContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#typedef_name}.
	 * @param ctx the parse tree
	 */
	void exitTypedef_name(@NotNull C2014Parser.Typedef_nameContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#equality_expression}.
	 * @param ctx the parse tree
	 */
	void enterEquality_expression(@NotNull C2014Parser.Equality_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#equality_expression}.
	 * @param ctx the parse tree
	 */
	void exitEquality_expression(@NotNull C2014Parser.Equality_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#plain_declaration}.
	 * @param ctx the parse tree
	 */
	void enterPlain_declaration(@NotNull C2014Parser.Plain_declarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#plain_declaration}.
	 * @param ctx the parse tree
	 */
	void exitPlain_declaration(@NotNull C2014Parser.Plain_declarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#string}.
	 * @param ctx the parse tree
	 */
	void enterString(@NotNull C2014Parser.StringContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#string}.
	 * @param ctx the parse tree
	 */
	void exitString(@NotNull C2014Parser.StringContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#init_declarators}.
	 * @param ctx the parse tree
	 */
	void enterInit_declarators(@NotNull C2014Parser.Init_declaratorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#init_declarators}.
	 * @param ctx the parse tree
	 */
	void exitInit_declarators(@NotNull C2014Parser.Init_declaratorsContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#integer_constant}.
	 * @param ctx the parse tree
	 */
	void enterInteger_constant(@NotNull C2014Parser.Integer_constantContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#integer_constant}.
	 * @param ctx the parse tree
	 */
	void exitInteger_constant(@NotNull C2014Parser.Integer_constantContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#inclusive_or_expression}.
	 * @param ctx the parse tree
	 */
	void enterInclusive_or_expression(@NotNull C2014Parser.Inclusive_or_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#inclusive_or_expression}.
	 * @param ctx the parse tree
	 */
	void exitInclusive_or_expression(@NotNull C2014Parser.Inclusive_or_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#selection_statement}.
	 * @param ctx the parse tree
	 */
	void enterSelection_statement(@NotNull C2014Parser.Selection_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#selection_statement}.
	 * @param ctx the parse tree
	 */
	void exitSelection_statement(@NotNull C2014Parser.Selection_statementContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#constant_expression}.
	 * @param ctx the parse tree
	 */
	void enterConstant_expression(@NotNull C2014Parser.Constant_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#constant_expression}.
	 * @param ctx the parse tree
	 */
	void exitConstant_expression(@NotNull C2014Parser.Constant_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#statement}.
	 * @param ctx the parse tree
	 */
	void enterStatement(@NotNull C2014Parser.StatementContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#statement}.
	 * @param ctx the parse tree
	 */
	void exitStatement(@NotNull C2014Parser.StatementContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#shift_expression}.
	 * @param ctx the parse tree
	 */
	void enterShift_expression(@NotNull C2014Parser.Shift_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#shift_expression}.
	 * @param ctx the parse tree
	 */
	void exitShift_expression(@NotNull C2014Parser.Shift_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#postfix}.
	 * @param ctx the parse tree
	 */
	void enterPostfix(@NotNull C2014Parser.PostfixContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#postfix}.
	 * @param ctx the parse tree
	 */
	void exitPostfix(@NotNull C2014Parser.PostfixContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExpression(@NotNull C2014Parser.ExpressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExpression(@NotNull C2014Parser.ExpressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#type_specifier}.
	 * @param ctx the parse tree
	 */
	void enterType_specifier(@NotNull C2014Parser.Type_specifierContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#type_specifier}.
	 * @param ctx the parse tree
	 */
	void exitType_specifier(@NotNull C2014Parser.Type_specifierContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#expression_statement}.
	 * @param ctx the parse tree
	 */
	void enterExpression_statement(@NotNull C2014Parser.Expression_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#expression_statement}.
	 * @param ctx the parse tree
	 */
	void exitExpression_statement(@NotNull C2014Parser.Expression_statementContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#assignment_expression}.
	 * @param ctx the parse tree
	 */
	void enterAssignment_expression(@NotNull C2014Parser.Assignment_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#assignment_expression}.
	 * @param ctx the parse tree
	 */
	void exitAssignment_expression(@NotNull C2014Parser.Assignment_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#character_constant}.
	 * @param ctx the parse tree
	 */
	void enterCharacter_constant(@NotNull C2014Parser.Character_constantContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#character_constant}.
	 * @param ctx the parse tree
	 */
	void exitCharacter_constant(@NotNull C2014Parser.Character_constantContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#declaration}.
	 * @param ctx the parse tree
	 */
	void enterDeclaration(@NotNull C2014Parser.DeclarationContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#declaration}.
	 * @param ctx the parse tree
	 */
	void exitDeclaration(@NotNull C2014Parser.DeclarationContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#declarator}.
	 * @param ctx the parse tree
	 */
	void enterDeclarator(@NotNull C2014Parser.DeclaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#declarator}.
	 * @param ctx the parse tree
	 */
	void exitDeclarator(@NotNull C2014Parser.DeclaratorContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#initializer}.
	 * @param ctx the parse tree
	 */
	void enterInitializer(@NotNull C2014Parser.InitializerContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#initializer}.
	 * @param ctx the parse tree
	 */
	void exitInitializer(@NotNull C2014Parser.InitializerContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#shift_operator}.
	 * @param ctx the parse tree
	 */
	void enterShift_operator(@NotNull C2014Parser.Shift_operatorContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#shift_operator}.
	 * @param ctx the parse tree
	 */
	void exitShift_operator(@NotNull C2014Parser.Shift_operatorContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#declarators}.
	 * @param ctx the parse tree
	 */
	void enterDeclarators(@NotNull C2014Parser.DeclaratorsContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#declarators}.
	 * @param ctx the parse tree
	 */
	void exitDeclarators(@NotNull C2014Parser.DeclaratorsContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#logical_and_expression}.
	 * @param ctx the parse tree
	 */
	void enterLogical_and_expression(@NotNull C2014Parser.Logical_and_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#logical_and_expression}.
	 * @param ctx the parse tree
	 */
	void exitLogical_and_expression(@NotNull C2014Parser.Logical_and_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#plain_declarator}.
	 * @param ctx the parse tree
	 */
	void enterPlain_declarator(@NotNull C2014Parser.Plain_declaratorContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#plain_declarator}.
	 * @param ctx the parse tree
	 */
	void exitPlain_declarator(@NotNull C2014Parser.Plain_declaratorContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#arguments}.
	 * @param ctx the parse tree
	 */
	void enterArguments(@NotNull C2014Parser.ArgumentsContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#arguments}.
	 * @param ctx the parse tree
	 */
	void exitArguments(@NotNull C2014Parser.ArgumentsContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#relational_expression}.
	 * @param ctx the parse tree
	 */
	void enterRelational_expression(@NotNull C2014Parser.Relational_expressionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#relational_expression}.
	 * @param ctx the parse tree
	 */
	void exitRelational_expression(@NotNull C2014Parser.Relational_expressionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#function_definition}.
	 * @param ctx the parse tree
	 */
	void enterFunction_definition(@NotNull C2014Parser.Function_definitionContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#function_definition}.
	 * @param ctx the parse tree
	 */
	void exitFunction_definition(@NotNull C2014Parser.Function_definitionContext ctx);

	/**
	 * Enter a parse tree produced by {@link C2014Parser#jump_statement}.
	 * @param ctx the parse tree
	 */
	void enterJump_statement(@NotNull C2014Parser.Jump_statementContext ctx);
	/**
	 * Exit a parse tree produced by {@link C2014Parser#jump_statement}.
	 * @param ctx the parse tree
	 */
	void exitJump_statement(@NotNull C2014Parser.Jump_statementContext ctx);
}
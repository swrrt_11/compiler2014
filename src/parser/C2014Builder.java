package parser;
import java.util.*;
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.ParserRuleContext;
import parser.C2014Parser.Character_constantContext;
import parser.C2014Parser.Init_declaratorContext;
import parser.C2014Parser.Init_declaratorsContext;
import parser.C2014Parser.InitializerContext;
import parser.C2014Parser.*;
import ast.*;
import ast.Op.OpType;
public class C2014Builder extends C2014BaseVisitor<Object>{
	void error(int rule){
		System.out.println(C2014Parser.ruleNames[rule]);
	}
	boolean isLeaf(ParseTree t){
		return (t.getChildCount()==0);
	}
	@Override
	public Program visitProgram(ProgramContext ctx){
		Program ret=new Program();
		DeclList declList=new DeclList();
		for(int i=0;i<ctx.getChildCount();i++)
			if(!isLeaf(ctx.getChild(i))){	
				ParserRuleContext now = (ParserRuleContext)ctx.getChild(i);
				if(now.getRuleIndex()==C2014Parser.RULE_declaration)declList.update(visitDeclaration((DeclarationContext)now));
				else if(now.getRuleIndex()==C2014Parser.RULE_function_definition){
					ret.son.add(declList);
					ret.son.add(visitFunction_definition((Function_definitionContext)now));
					declList=new DeclList();
				}else error(C2014Parser.RULE_program);
			}
		ret.son.add(declList);
		return ret;
	}
	@Override
	public DeclList visitDeclaration(DeclarationContext ctx){
		DeclList ret=new DeclList();
		Type type=visitType_specifier((Type_specifierContext)ctx.getChild(0));
		List<DeclInit>decl=new ArrayList<DeclInit>();
		if(ctx.getChildCount()==3)decl=visitInit_declarators((Init_declaratorsContext)ctx.getChild(1));
		else decl.add(new DeclInit());
		ret.InitUpdate(type,decl);
		return ret;
	}
	@Override
	public Func visitFunction_definition(Function_definitionContext ctx){
		Func ret=new Func();
		for(int i=0;i<ctx.getChildCount();i++)
			if(!isLeaf(ctx.getChild(i))){
				ParserRuleContext now=(ParserRuleContext)ctx.getChild(i);
				if(now.getRuleIndex()==C2014Parser.RULE_type_specifier)ret.type=visitType_specifier((Type_specifierContext)now);
				else if(now.getRuleIndex()==C2014Parser.RULE_plain_declarator){
					Decl tmp=visitPlain_declarator((Plain_declaratorContext)now);
					for(int j=0;j<tmp.pointer;j++)ret.type=new Pointer(ret.type);
					ret.name=tmp.name;
				}
				else if(now.getRuleIndex()==C2014Parser.RULE_parameters)ret.arg=visitParameters((ParametersContext)now);
				else if(now.getRuleIndex()==C2014Parser.RULE_compound_statement)ret.block=visitCompound_statement((Compound_statementContext)now);
				else error(C2014Parser.RULE_function_definition);
			}
		return ret;
	}
	@Override
	public Arg visitParameters(ParametersContext ctx){
		Arg ret=new Arg();
		for(int i=0;i<ctx.getChildCount();i++){
			if(isLeaf(ctx.getChild(i))){
				TerminalNode now=(TerminalNode)ctx.getChild(i);
				if(now.getSymbol().getText().equals("..."))ret.infinity=true;
			}else{
				ParserRuleContext now=(ParserRuleContext)ctx.getChild(i);
				if(now.getRuleIndex()==C2014Parser.RULE_plain_declaration){
					Var var=visitPlain_declaration((Plain_declarationContext)now);
					ret.list.add(var);
				}else error(C2014Parser.RULE_plain_declaration);
			}
		}
		return ret;
	}
	@Override
	public List<Decl> visitDeclarators(DeclaratorsContext ctx){
		List<Decl> ret=new ArrayList<Decl>();
		for(int i=0;i<ctx.getChildCount();i++)
			if(!isLeaf(ctx.getChild(i))){
				ParserRuleContext now=(ParserRuleContext)ctx.getChild(i);
				if(now.getRuleIndex()==C2014Parser.RULE_declarator)ret.add(visitDeclarator((DeclaratorContext)now));
				else error(C2014Parser.RULE_declarator);
			}
		return ret;
	}
	@Override
	public List<DeclInit> visitInit_declarators(Init_declaratorsContext ctx){
		List<DeclInit> ret=new ArrayList<DeclInit>();
		for(int i=0;i<ctx.getChildCount();i++)
			if(!isLeaf(ctx.getChild(i))){
				ParserRuleContext now=(ParserRuleContext)ctx.getChild(i);
				if(now.getRuleIndex()==C2014Parser.RULE_init_declarator)ret.add(visitInit_declarator((Init_declaratorContext)now));
				else error(C2014Parser.RULE_init_declarator);
			}
		return ret;
	}
	@Override
	public DeclInit visitInit_declarator(Init_declaratorContext ctx){
		DeclInit ret=new DeclInit();
		ret.decl=visitDeclarator((DeclaratorContext)ctx.getChild(0));
		if(ctx.getChildCount()==3)ret.init=visitInitializer((InitializerContext)ctx.getChild(2));
		return ret;
	}
	@Override
	public Init visitInitializer(InitializerContext ctx){
		Init ret=new Init();
		if(ctx.getChildCount()==1)return (new Terminal(visitAssignment_expression((Assignment_expressionContext)ctx.getChild(0))));
		else {
			for(int i=1;i<ctx.getChildCount();i++)
				if(!isLeaf(ctx.getChild(i))){
					ParserRuleContext now=(ParserRuleContext)ctx.getChild(i);
					if(now.getRuleIndex()==C2014Parser.RULE_initializer)ret.son.add(visitInitializer((InitializerContext)now));
					else error(C2014Parser.RULE_initializer);
				}
			}
		return ret;
	}
	@Override
	public Type visitType_specifier(Type_specifierContext ctx){
		Type ret=new Type();
		if(ctx.getChildCount()==1){
			TerminalNode now=(TerminalNode)ctx.getChild(0);
			if(now.getSymbol().getType()==C2014Parser.Void)ret.name=Type.TypeName.Void;
			else if(now.getSymbol().getType()==C2014Parser.Char)ret.name=Type.TypeName.Char;
			else if(now.getSymbol().getType()==C2014Parser.Int)ret.name=Type.TypeName.Int;
			else error(C2014Parser.RULE_type_specifier);
		}else{
			StructUnion new_ret=new StructUnion();
			new_ret.name=visitStruct_or_union((Struct_or_unionContext)ctx.getChild(0));
			if(ctx.getChildCount()==2)new_ret.alias=visitIdentifier((IdentifierContext)ctx.getChild(1));
			else{
				new_ret.body=new DeclList();
				Type fa=new Type();
				for(int i=1;i<ctx.getChildCount();i++)
					if(!isLeaf(ctx.getChild(i))){
						ParserRuleContext now=(ParserRuleContext)ctx.getChild(i);
						if(now.getRuleIndex()==C2014Parser.RULE_identifier)new_ret.alias=visitIdentifier((IdentifierContext)now);
						else if(now.getRuleIndex()==C2014Parser.RULE_type_specifier)fa=visitType_specifier((Type_specifierContext)now);
						else if(now.getRuleIndex()==C2014Parser.RULE_declarators)new_ret.body.DeclUpdate(fa,visitDeclarators((DeclaratorsContext)now));
						else error(C2014Parser.RULE_type_specifier);
					}
			}
			ret=new_ret;
		}
		return ret;
	}
	@Override
	public Type.TypeName visitStruct_or_union(Struct_or_unionContext ctx){
		TerminalNode now=(TerminalNode)ctx.getChild(0);
		if(now.getSymbol().getType()==C2014Parser.Struct)return Type.TypeName.Struct;
		if(now.getSymbol().getType()==C2014Parser.Union)return Type.TypeName.Union;
		return null;
	}
	@Override
	public Var visitPlain_declaration(Plain_declarationContext ctx){
		Type type=visitType_specifier((Type_specifierContext)ctx.getChild(0));
		Decl decl=visitDeclarator((DeclaratorContext)ctx.getChild(1));
		for(int i=0;i<decl.pointer;i++)type=new Pointer(type);
		if(decl.kind==Node.Kind.DeclFunc)type=new Function(type,((DeclFunc)decl).arg);
		else if(decl.kind==Node.Kind.DeclArray){
			DeclArray tmp=(DeclArray)decl;
			for(Expr i:tmp.array)type=new Array(type,i);
		}
		return (new Var(type,decl.name,new Init()));
	}
	@Override
	public Decl visitDeclarator(DeclaratorContext ctx){
		Decl ret=visitPlain_declarator((Plain_declaratorContext)ctx.getChild(0));
		boolean is=false;
		Arg arg=new Arg();
		List<Expr>a=new ArrayList<Expr>();
		//System.out.println("Fuck");
		//System.out.printf("%d",ctx.getChildCount());
		for(int i=1;i<ctx.getChildCount();i++)
			if(isLeaf(ctx.getChild(i))){
				//System.out.println("Fuck u");
				TerminalNode now=(TerminalNode)ctx.getChild(i);
				if(now.getSymbol().getText().equals("("))is=true;
			}else{
				//System.out.println("Fuck u");
				ParserRuleContext now=(ParserRuleContext)ctx.getChild(i);
				if(now.getRuleIndex()==C2014Parser.RULE_parameters)arg=visitParameters((ParametersContext)now);
				else if(now.getRuleIndex()==C2014Parser.RULE_constant_expression)a.add(visitConstant_expression((Constant_expressionContext)now));
				else error(C2014Parser.RULE_declarator);
			}
		if(is)return new DeclFunc(ret,arg);
		else if(!a.isEmpty())return new DeclArray(ret,a);
		return ret;
	}
	@Override
	public Decl visitPlain_declarator(Plain_declaratorContext ctx){
		Decl ret=new Decl();
		int gs=0;
		Id id=new Id();
		for(int i=0;i<ctx.getChildCount();i++)
			if(isLeaf(ctx.getChild(i))){
				if(ctx.getChild(i).getText().equals("*"))gs++;
				else error(C2014Parser.RULE_plain_declarator);
			}else{
				ParserRuleContext now=(ParserRuleContext)ctx.getChild(i);
				if(now.getRuleIndex()==C2014Parser.RULE_identifier)id=visitIdentifier((IdentifierContext)now);
				else error(C2014Parser.RULE_plain_declarator);
			}
		ret=new Decl(gs,id);
		return ret;
	}
	@Override
	public Stat visitStatement(StatementContext ctx){
		Stat ret=null;
		ParserRuleContext now=(ParserRuleContext)ctx.getChild(0);
		if(now.getRuleIndex()==C2014Parser.RULE_expression_statement)ret=visitExpression_statement((Expression_statementContext)now);
		else if(now.getRuleIndex()==C2014Parser.RULE_compound_statement)ret=visitCompound_statement((Compound_statementContext)now);
		else if(now.getRuleIndex()==C2014Parser.RULE_selection_statement)ret=visitSelection_statement((Selection_statementContext)now);
		else if(now.getRuleIndex()==C2014Parser.RULE_iteration_statement)ret=visitIteration_statement((Iteration_statementContext)now);
		else if(now.getRuleIndex()==C2014Parser.RULE_jump_statement)ret=visitJump_statement((Jump_statementContext)now);
		else error(C2014Parser.RULE_statement);
		return ret;
	}
	@Override
	public Stat visitExpression_statement(Expression_statementContext ctx){
		Stat ret=new Stat();
		if(ctx.getChildCount()>1){
			ret=visitExpression((ExpressionContext)ctx.getChild(0));
		}
		return ret;
	}
	@Override
	public Block visitCompound_statement(Compound_statementContext ctx){
		Block ret=new Block();
		for(int i=1;i<ctx.getChildCount()-1;i++){
			ParserRuleContext now=(ParserRuleContext)ctx.getChild(i);
			if(now.getRuleIndex()==C2014Parser.RULE_declaration)ret.decl.update(visitDeclaration((DeclarationContext)now));
			else if(now.getRuleIndex()==C2014Parser.RULE_statement)ret.stat.add(visitStatement((StatementContext)now));
			else error(C2014Parser.RULE_compound_statement);
		}
		return ret;
	}
	@Override
	public If visitSelection_statement(Selection_statementContext ctx){
		If ret=new If();
		ret.expr=visitExpression((ExpressionContext)ctx.getChild(2));
		ret.Then=visitStatement((StatementContext)ctx.getChild(4));
		if(ctx.getChildCount()==7)ret.Else=visitStatement((StatementContext)ctx.getChild(6));
		return ret;
	}
	@Override
	public For visitIteration_statement(Iteration_statementContext ctx){
		For ret=new For();
		if(((TerminalNode)ctx.getChild(0)).getSymbol().getType()==C2014Parser.While){
			ret.expr=visitExpression((ExpressionContext)ctx.getChild(2));
			ret.stat=visitStatement((StatementContext)ctx.getChild(4));
		}else if(((TerminalNode)ctx.getChild(0)).getSymbol().getType()==C2014Parser.For){
			int gs=0;
			for(int i=1;i<ctx.getChildCount();i++)
				if(isLeaf(ctx.getChild(i))){
					if(((TerminalNode)ctx.getChild(i)).getSymbol().getText().equals(";"))gs++;
				}else{
					ParserRuleContext now=(ParserRuleContext)ctx.getChild(i);
					if(now.getRuleIndex()==C2014Parser.RULE_expression){
						Expr expr=visitExpression((ExpressionContext)now);
						if(gs==0)ret.init=expr;
						else if(gs==1)ret.expr=expr;
						else ret.iter=expr;
					}else{
						ret.stat=visitStatement((StatementContext)now);
					}
				}
		}
		return ret;
	}
	@Override
	public Jump visitJump_statement(Jump_statementContext ctx){
		Jump ret=new Jump();
		if(((TerminalNode)ctx.getChild(0)).getSymbol().getType()==C2014Parser.Continue)ret.type=Jump.JumpType.Continue;
		else if(((TerminalNode)ctx.getChild(0)).getSymbol().getType()==C2014Parser.Break)ret.type=Jump.JumpType.Break;
		else if(((TerminalNode)ctx.getChild(0)).getSymbol().getType()==C2014Parser.Return){
			ret.type=Jump.JumpType.Return;
			if(!isLeaf(ctx.getChild(1)))ret.expr=visitExpression((ExpressionContext)ctx.getChild(1));
		}
		return ret;
	}
	@Override
	public Expr visitExpression(ExpressionContext ctx){
		Expr ret=new Expr();
		for(int i=0;i<ctx.getChildCount();i++)
			if(!isLeaf(ctx.getChild(i))){
				ret.expr.add(visitAssignment_expression((Assignment_expressionContext)ctx.getChild(i)));
			}
		return ret;
	}
	@Override
	public Expr visitAssignment_expression(Assignment_expressionContext ctx){
		if(ctx.getChildCount()==1)return visitLogical_or_expression((Logical_or_expressionContext)ctx.getChild(0));
		Op ret=new Op();
		ret.left=visitUnary_expression((Unary_expressionContext)ctx.getChild(0));
		ret.op=visitAssignment_operator((Assignment_operatorContext)ctx.getChild(1));
		ret.right=visitAssignment_expression((Assignment_expressionContext)ctx.getChild(2));
		return ret;
	}
	@Override
	public Op.OpType visitAssignment_operator(Assignment_operatorContext ctx){
		String t=((TerminalNode)ctx.getChild(0)).getSymbol().getText();
		if(t.equals("="))return OpType.Assign;
		if(t.equals("*="))return OpType.MulAss;
		if(t.equals("/="))return OpType.DivAss;
		if(t.equals("%="))return OpType.ModAss;
		if(t.equals("+="))return OpType.AddAss;
		if(t.equals("-="))return OpType.SubAss;
		if(t.equals("<<="))return OpType.LShiftAss;
		if(t.equals(">>="))return OpType.RShiftAss;
		if(t.equals("&="))return OpType.BitAndAss;
		if(t.equals("|="))return OpType.BitOrAss;
		if(t.equals("^="))return OpType.BitXorAss;
		return null;
	}
	@Override
	public Expr visitConstant_expression(Constant_expressionContext ctx){
		return visitLogical_or_expression((Logical_or_expressionContext)ctx.getChild(0));
	}
	@Override
	public Expr visitLogical_or_expression(Logical_or_expressionContext ctx){
		Expr ret=visitLogical_and_expression((Logical_and_expressionContext)ctx.getChild(0));
		for(int i=2;i<ctx.getChildCount();i+=2){
			ret=new Op(ret,OpType.LogOr,visitLogical_and_expression((Logical_and_expressionContext)ctx.getChild(i)));
		}
		return ret;
	}
	@Override
	public Expr visitLogical_and_expression(Logical_and_expressionContext ctx){
		Expr ret=visitInclusive_or_expression((Inclusive_or_expressionContext)ctx.getChild(0));
		for(int i=2;i<ctx.getChildCount();i+=2){
			ret=new Op(ret,OpType.LogAnd,visitInclusive_or_expression((Inclusive_or_expressionContext)ctx.getChild(i)));
		}
		return ret;
	}
	@Override
	public Expr visitInclusive_or_expression(Inclusive_or_expressionContext ctx){
		Expr ret=visitExclusive_or_expression((Exclusive_or_expressionContext)ctx.getChild(0));
		for(int i=2;i<ctx.getChildCount();i+=2){
			ret=new Op(ret,OpType.BitOr,visitExclusive_or_expression((Exclusive_or_expressionContext)ctx.getChild(i)));
		}
		return ret;
	}
	@Override
	public Expr visitExclusive_or_expression(Exclusive_or_expressionContext ctx){
		Expr ret=visitAnd_expression((And_expressionContext)ctx.getChild(0));
		for(int i=2;i<ctx.getChildCount();i+=2){
			ret=new Op(ret,OpType.BitXor,visitAnd_expression((And_expressionContext)ctx.getChild(i)));
		}
		return ret;
	}
	@Override
	public Expr visitAnd_expression(And_expressionContext ctx){
		Expr ret=visitEquality_expression((Equality_expressionContext)ctx.getChild(0));
		for(int i=2;i<ctx.getChildCount();i+=2){
			ret=new Op(ret,OpType.BitAnd,visitEquality_expression((Equality_expressionContext)ctx.getChild(i)));
		}
		return ret;
	}
	@Override
	public Expr visitEquality_expression(Equality_expressionContext ctx){
		Expr ret=visitRelational_expression((Relational_expressionContext)ctx.getChild(0));
		for(int i=1;i<ctx.getChildCount();i+=2){
			Op.OpType op=visitEquality_operator((Equality_operatorContext)ctx.getChild(i));
			ret=new Op(ret,op,visitRelational_expression((Relational_expressionContext)ctx.getChild(i+1)));
		}
		return ret;
	}
	@Override
	public Op.OpType visitEquality_operator(Equality_operatorContext ctx){
		if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals("=="))return Op.OpType.Equal;
		return Op.OpType.NotEqual;
	}
	@Override
	public Expr visitRelational_expression(Relational_expressionContext ctx){
		Expr ret=visitShift_expression((Shift_expressionContext)ctx.getChild(0));
		for(int i=1;i<ctx.getChildCount();i+=2){
			Op.OpType op=visitRelational_operator((Relational_operatorContext)ctx.getChild(i));
			ret=new Op(ret,op,visitShift_expression((Shift_expressionContext)ctx.getChild(i+1)));
		}
		return ret;
	}
	@Override
	public Op.OpType visitRelational_operator(Relational_operatorContext ctx){
		if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals("<"))return Op.OpType.Less;
		if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals(">"))return Op.OpType.Greater;
		if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals("<="))return Op.OpType.LEqual;
		if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals(">="))return Op.OpType.LEqual;
		return null;
	}
	@Override
	public Expr visitShift_expression(Shift_expressionContext ctx){
		Expr ret=visitAdditive_expression((Additive_expressionContext)ctx.getChild(0));
		for(int i=1;i<ctx.getChildCount();i+=2){
			Op.OpType op=visitShift_operator((Shift_operatorContext)ctx.getChild(i));
			ret=new Op(ret,op,visitAdditive_expression((Additive_expressionContext)ctx.getChild(i+1)));
		}
		return ret;
	}
	@Override
	public Op.OpType visitShift_operator(Shift_operatorContext ctx){
		if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals("<<"))return Op.OpType.LShift;
		if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals(">>"))return Op.OpType.LShift;
		return null;
	}
	@Override
	public Expr visitAdditive_expression(Additive_expressionContext ctx){
		Expr ret=visitMultiplicative_expression((Multiplicative_expressionContext)ctx.getChild(0));
		for(int i=1;i<ctx.getChildCount();i+=2){
			Op.OpType op=visitAdditive_operator((Additive_operatorContext)ctx.getChild(i));
			ret=new Op(ret,op,visitMultiplicative_expression((Multiplicative_expressionContext)ctx.getChild(i+1)));
		}
		return ret;
	}
	@Override
	public Op.OpType visitAdditive_operator(Additive_operatorContext ctx){
		if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals("+"))return Op.OpType.Add;
		if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals("-"))return Op.OpType.Sub;
		return null;
	}
	@Override
	public Expr visitMultiplicative_expression(Multiplicative_expressionContext ctx){
		Expr ret=visitCast_expression((Cast_expressionContext)ctx.getChild(0));
		for(int i=1;i<ctx.getChildCount();i+=2){
			Op.OpType op=visitMultiplicative_operator((Multiplicative_operatorContext)ctx.getChild(i));
			ret=new Op(ret,op,visitCast_expression((Cast_expressionContext)ctx.getChild(i+1)));
		}
		return ret;
	}
	@Override
	public Op.OpType visitMultiplicative_operator(Multiplicative_operatorContext ctx){
		if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals("*"))return Op.OpType.Mul;
		if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals("/"))return Op.OpType.Div;
		if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals("%"))return Op.OpType.Mod;
		return null;
	}
	@Override
	public Expr visitCast_expression(Cast_expressionContext ctx){
		if(isLeaf(ctx.getChild(0))){
			Type type=visitType_name((Type_nameContext)ctx.getChild(1));
			Expr expr=visitCast_expression((Cast_expressionContext)ctx.getChild(3));
			return new Transform(expr,type);
		}else{
			return visitUnary_expression((Unary_expressionContext)ctx.getChild(0));
		}
		
	}
	@Override
	public Type visitType_name(Type_nameContext ctx){
		Type ret=visitType_specifier((Type_specifierContext)ctx.getChild(0));
		for(int i=1;i<ctx.getChildCount();i+=2){
			if(((TerminalNode)ctx.getChild(i)).getSymbol().getText().equals("*"))ret=new Pointer(ret);
		}
		return ret;
	}
	@Override
	public Expr visitUnary_expression(Unary_expressionContext ctx){
		if(ctx.getChildCount()==1)return visitPostfix_expression((Postfix_expressionContext)ctx.getChild(0));
		else if(ctx.getChildCount()==4)return new Sizeof(visitType_name((Type_nameContext)ctx.getChild(2)));
		else if(isLeaf(ctx.getChild(0))){
			Expr expr=visitUnary_expression((Unary_expressionContext)ctx.getChild(1));
			if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals("++"))return new Prefix(Prefix.OpType.Add,expr);
			else if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals("--"))return new Prefix(Prefix.OpType.Sub,expr);
			else if(((TerminalNode)ctx.getChild(0)).getSymbol().getType()==C2014Parser.Sizeof)return new Prefix(Prefix.OpType.Sizeof,expr);
		}else{
			Expr expr=visitCast_expression((Cast_expressionContext)ctx.getChild(1));
			Prefix.OpType op=visitUnary_operator((Unary_operatorContext)ctx.getChild(0));
			return new Prefix(op,expr);
		}
		return null;
	}
	@Override
	public Prefix.OpType visitUnary_operator(Unary_operatorContext ctx){
		String op=((TerminalNode)ctx.getChild(0)).getSymbol().getText();
		if(op.equals("&"))return Prefix.OpType.Address;
		if(op.equals("*"))return Prefix.OpType.Pointer;
		if(op.equals("+"))return Prefix.OpType.Positive;
		if(op.equals("-"))return Prefix.OpType.Negative;
		if(op.equals("~"))return Prefix.OpType.BitNot;
		if(op.equals("!"))return Prefix.OpType.LogNot;
		return null;
	}
	@Override
	public Expr visitPostfix_expression(Postfix_expressionContext ctx){
		Expr ret=visitPrimary_expression((Primary_expressionContext)ctx.getChild(0));
		for(int i=1;i<ctx.getChildCount();i++){
			Postfix tmp=visitPostfix((PostfixContext)ctx.getChild(i));
			tmp.expr=ret;
			ret=tmp;
		}
		return ret;
	}
	@Override
	public Postfix visitPostfix(PostfixContext ctx){
		if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals("[")){
			Expr expr=visitExpression((ExpressionContext)ctx.getChild(1));
			return new Index(expr);
		}else if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals("(")){
			Para para=new Para();
			if(!isLeaf(ctx.getChild(1)))para=visitArguments((ArgumentsContext)ctx.getChild(1));
			return new Call(para);
		}else if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals(".")){
			Id id=visitIdentifier((IdentifierContext)ctx.getChild(1));
			return new Get(Get.GetType.Direct,id);
		}else if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals("->")){
			Id id=visitIdentifier((IdentifierContext)ctx.getChild(1));
			return new Get(Get.GetType.Indirect,id);
		}else if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals("++"))return new Postfix(Postfix.OpType.Add);
		else if(((TerminalNode)ctx.getChild(0)).getSymbol().getText().equals("--"))return new Postfix(Postfix.OpType.Sub);
		else error(C2014Parser.RULE_postfix); 
		return null;
	}
	@Override
	public Para visitArguments(ArgumentsContext ctx){
		Para ret=new Para();
		for(int i=0;i<ctx.getChildCount();i+=2){
			ret.para.add(visitAssignment_expression((Assignment_expressionContext)ctx.getChild(i)));
		}
		return ret;
	}
	@Override
	public Expr visitPrimary_expression(Primary_expressionContext ctx){
		if(ctx.getChildCount()==3)return visitExpression((ExpressionContext)ctx.getChild(1)); 
		ParserRuleContext now=((ParserRuleContext)ctx.getChild(0));
		if(now.getRuleIndex()==C2014Parser.RULE_identifier)return new Variable(visitIdentifier((IdentifierContext)now));
		else if(now.getRuleIndex()==C2014Parser.RULE_constant)return visitConstant((ConstantContext)now);
		else if(now.getRuleIndex()==C2014Parser.RULE_string)return new Str(visitString((StringContext)now));
		return null;
	}
	@Override
	public Expr visitConstant(ConstantContext ctx){
		ParserRuleContext now=((ParserRuleContext)ctx.getChild(0));
		if(now.getRuleIndex()==C2014Parser.RULE_integer_constant)return new Int(visitInteger_constant((Integer_constantContext)now));
		if(now.getRuleIndex()==C2014Parser.RULE_character_constant)return new Char(visitCharacter_constant((Character_constantContext)now));
		return null;
	}
	@Override
	public Id visitIdentifier(IdentifierContext ctx){
		TerminalNode now=(TerminalNode)ctx.getChild(0);
		if(now.getSymbol().getType()==C2014Parser.ID)return new Id(now.getSymbol().getText());
		return null;
	}
	@Override
	public Integer visitInteger_constant(Integer_constantContext ctx){
		int ret=0;
		TerminalNode now=(TerminalNode)ctx.getChild(0);
		String tmp=now.getSymbol().getText();
		if(now.getSymbol().getType()==C2014Parser.DEC){
			for(int i=0;i<tmp.length();i++){
				ret*=10;ret+=tmp.charAt(i)-'0';
			}
		}else if(now.getSymbol().getType()==C2014Parser.OCT){
			for(int i=1;i<tmp.length();i++){
				ret*=8;ret+=tmp.charAt(i)-'0';
			}
		}else if(now.getSymbol().getType()==C2014Parser.HEX){
			for(int i=2;i<tmp.length();i++){
				ret*=16;
				if(Character.isDigit(tmp.charAt(i)))ret+=tmp.charAt(i);
				else ret+=Character.toUpperCase(tmp.charAt(i))-'A'+10;
			}
		}
		return ret;
	}
	char changeSlash(char ch){
		if(ch=='a')return (char)7;
		if(ch=='b')return (char)8;
		if(ch=='f')return (char)12;
		if(ch=='n')return (char)10;
		if(ch=='r')return (char)13;
		if(ch=='t')return (char)9;
		if(ch=='w')return (char)11;
		if(ch=='\\')return (char)92;
		if(ch=='\'')return (char)39;
		if(ch=='\"')return (char)34;
		if(ch=='0')return (char)0;
		return ch;
	}
	@Override
	public Character visitCharacter_constant(Character_constantContext ctx){
		char ret='\0';
		String tmp=((TerminalNode)ctx.getChild(0)).getSymbol().getText();
		if(tmp.charAt(1)=='\\'){
			if(tmp.length()==4)ret=changeSlash(tmp.charAt(2));
			else if(tmp.charAt(2)!='x'){
				ret=(char)((int)(tmp.charAt(2)-'0')*100+(int)(tmp.charAt(3)-'0')*10+(int)(tmp.charAt(4)-'0'));
			}else {
				ret=(char)((int)(tmp.charAt(3)-'0')*16+(int)(tmp.charAt(4)-'0'));
			}
		}
		else ret=tmp.charAt(1);
		return ret;
	}
	@Override
	public String visitString(StringContext ctx){
		TerminalNode now=(TerminalNode)ctx.getChild(0);
		String tmp=now.getSymbol().getText(),ret="";
		for(int i=1;i<tmp.length()-1;i++)
			if(tmp.charAt(i)=='\\'){
				ret+=changeSlash(tmp.charAt(i+1));
				i++;
			}else ret+=tmp.charAt(i);
		return ret;
	}
}

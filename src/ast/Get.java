package ast;
import java.io.*;

import symbol.*;
public class Get extends Postfix{
	public static enum GetType{
		Direct,Indirect
	}
	public GetType way;
	public Id id;
	public Get(){
		op=Postfix.OpType.Get;
		kind=Node.Kind.Postfix;
	}
	public Get(GetType _way,Id _id){
		id=_id;
		way=_way;
		op=Postfix.OpType.Get;
		kind=Node.Kind.Postfix;
	}
	@Override
	public void print(PrintStream out,int len){
		super.print(out,len);
		space(out,len+1);
		out.println(way);
		id.print(out,len+1);
	}
	@Override
	public void update(Table table)throws Exception{
		expr.update(table);
		if(way==GetType.Direct){
			Type now=expr.type;

			if(now.name==Type.TypeName.Alias)now=table.getType(((Alias)now).alias);
			if(!isStruct(now))error("Not a Struct");
			if(StructUnion.class.isInstance(now)){
				StructUnion tmp=(StructUnion)now;
				if(tmp.body.empty()&&(tmp.alias.equals("")||table.getType(tmp.alias)==null))error("Struct error");
				now=table.getType(tmp.alias);
			}
			if(now instanceof StructUnion)now=new Struct((StructUnion)now);
			type=((Struct)now).get(id);
			lvalue=isLvalue(type);
		}else{
			Type now=expr.type;
			if(!isPointer(now)&&!isStruct(((Pointer)now).type))error("Not a struct");
			now=((Pointer)now).type;
	//System.out.printf("%s %s\n",id.str,now.name.name());
			if(StructUnion.class.isInstance(now)){
				StructUnion tmp=(StructUnion)now;
				if(tmp.body.empty()&&(tmp.alias.equals("")||table.getType(tmp.alias)==null))error("Struct error");
	//System.out.printf("%s\n",tmp.alias.str);
				now=table.getType(tmp.alias);
				if(now==null)now=table.getVariable(tmp.alias).type;
				//if(tmp.alias.str.equals("A"))now=new StructUnion(Type.TypeName.Struct);
//	System.out.printf("%s %s\n",id.str,now);
			}
	//System.out.printf("%s %s\n",id.str,now);
			type=((Struct)now).get(id);
//			System.out.println(id.str);
			lvalue=isLvalue(type);
		}
	}
}

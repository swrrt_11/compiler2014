package ast;

import java.util.*;
public class Struct extends Type{
	public Dictionary<String,Type> body;
	public String alias;
	public Struct(){
		body=new Hashtable<String,Type>();
	}
	public Struct(StructUnion tmp,int p){
//System.out.println("Z1");
		name=tmp.name;
		alias=tmp.alias.str;
		body=new Hashtable<String,Type>();
/*		for(Var i:tmp.body.list){
//System.out.printf("%s %s\n",i.name.str,i.type.name.name());
			
			body.put(i.name.str,i.type);
		}*/
		kind=Node.Kind.Type;
	}
	public Struct(StructUnion tmp)throws Exception{
//System.out.println("Z1");
		name=tmp.name;
		alias=tmp.alias.str;
		body=new Hashtable<String,Type>();
		for(Var i:tmp.body.list){
//System.out.printf("%s %s\n",i.name.str,i.type.name.name());
			//if(i.type)
			if(body.get(i.name.str)==null)body.put(i.name.str,i.type);
			else error("Struct define duplic");
		}
		kind=Node.Kind.Type;
	}
	public Type get(Id id)throws Exception{
	//System.out.println(id.str);
		Type ret=body.get(id.str);
		if(ret==null)error("Member not exist");
		return ret;
	}
	@Override
	public boolean same(Type x){
		if(x.name!=name)return false;
		Struct t=(Struct)x;
		if(alias.equals("")||t.alias.equals("")||!alias.equals(t.alias))return false;
		return true;
	}
}

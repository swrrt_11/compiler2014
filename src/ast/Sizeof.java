package ast;
import java.io.*;
import symbol.*;
public class Sizeof extends Expr{
	public Type sType;
	public Sizeof(){
		sType=new Type();
		kind=Node.Kind.Sizeof;
	}
	public Sizeof(Type _type){
		sType=_type;
		kind=Node.Kind.Sizeof;
	}
	@Override
	public void print(PrintStream out,int len){
		super.print(out,len);
		sType.print(out,len+1);
	}
	@Override
	public void update(Table table)throws Exception{
		type=new Type(Type.TypeName.Int);
		lvalue=false;
	}
}

package ast;
import java.io.*;
import symbol.*;
public class Op extends Expr{
	public static enum OpType{
		Add,Sub,Mul,Div,Mod,LogAnd,LogOr,BitAnd,BitOr,BitXor,LShift,RShift,Less,Greater,LEqual,GEqual,Equal,NotEqual,Assign,AddAss,SubAss,MulAss,DivAss,ModAss,LShiftAss,RShiftAss,BitAndAss,BitOrAss,BitXorAss
	}
	public Expr left,right;
	public OpType op;
	public Op(){
		left=right=new Expr();
		kind=Node.Kind.Op;
	}
	public Op(Expr _left,OpType _op,Expr _right){
		left=_left;
		right=_right;
		op=_op;
		kind=Node.Kind.Op;
	}
	@Override 
	public void print(PrintStream out,int len){
		super.print(out,len);
		left.print(out,len+1);
		space(out,len+1);
		out.println(op);
		right.print(out,len+1);
	}
	@Override
	public void update(Table table)throws Exception{
	//System.out.printf("ZZ1 %d\n",left.expr.size());	
	//if(left.==null)System.out.println("WTF\n");
		left.update(table);
	//System.out.printf("ZZ2\n");
		right.update(table);
	//System.out.printf("ZZ3\n");
	//System.out.printf("ZZ1 %s %s %s\n",left.type.name,op.name(),right.type.name);
		
		if(!OpUpdate()){
			System.err.printf("%s %s %s\n",left.type.name,op.name(),right.type.name);
			error("Expression error");
		}
	}
	public boolean isAdd(){
		return op==OpType.Add;
	}
	public boolean isSub(){
		return op==OpType.Sub;
	}
	public boolean isMul(){
		return op==OpType.Mul||op==OpType.Div||op==OpType.Mod;
	}
	public boolean isBit(){
		return op==OpType.BitAnd||op==OpType.BitOr||op==OpType.BitXor;
	}
	public boolean isLog(){
		return op==OpType.LogAnd||op==OpType.LogOr;
	}
	public boolean isShift(){
		return op==OpType.LShift||op==OpType.RShift;
	}
	public boolean isComp(){
		return op==OpType.Equal||op==OpType.LEqual||op==OpType.Less||op==OpType.GEqual||op==OpType.Greater||op==OpType.NotEqual;
	}
	public boolean isAssign(){
		return op==OpType.AddAss||op==OpType.Assign||op==OpType.SubAss||op==OpType.MulAss||op==OpType.DivAss||op==OpType.ModAss||op==OpType.BitAndAss||op==OpType.BitOrAss||op==OpType.BitXorAss||op==OpType.LShiftAss||op==OpType.RShiftAss;
	}
	public OpType removeAss(){
		if(op==OpType.AddAss)return OpType.Add;
		else if(op==OpType.SubAss)return OpType.Sub;
		else if(op==OpType.MulAss)return OpType.Mul;
		else if(op==OpType.DivAss)return OpType.Div;
		else if(op==OpType.ModAss)return OpType.Mod;
		else if(op==OpType.BitAndAss)return OpType.BitAnd;
		else if(op==OpType.BitOrAss)return OpType.BitOr;
		else if(op==OpType.BitXorAss)return OpType.BitXor;
		else if(op==OpType.LShiftAss)return OpType.LShiftAss;
		else if(op==OpType.RShiftAss)return OpType.RShiftAss;
		else return null;
	}
	public Type intOrchar(Type a,Type b){
		if(a.name==Type.TypeName.Char&&b.name==Type.TypeName.Char)return new Type(Type.TypeName.Char);
		return new Type(Type.TypeName.Int);
	}
	public boolean OpUpdate()throws Exception{
		lvalue=false;
		//System.out.printf("ZZZ\n");
		if(isAdd()||isLog()||isComp()){
			if((isArray(left.type)&&(isInt(right.type)||isArray(right.type)))){
				type=left.type;
				//if(isArray(type))type=new Type(Type.TypeName.Pointer);
				if(isComp()){
					type=new Type(Type.TypeName.Int);
				}
				return true;
			}
			if(isInt(left.type)&&isArray(right.type)){
				type=left.type;
				if(isArray(type))type=new Type(Type.TypeName.Pointer);
				if(isComp()){
					type=new Type(Type.TypeName.Int);
				}
				return true;
			}
			if(isInt(left.type)&&isInt(right.type)){
				type=intOrchar(left.type,right.type);
				if(isComp()){
					type=new Type(Type.TypeName.Int);
				}
				return true;
			}
			return false;
		}
		if(isSub()){
			if(isInt(left.type)&&isInt(right.type)){
				type=intOrchar(left.type,right.type);
				return true;
			}
			if(isArray(left.type)&&isArray(right.type)){
				type=new Type(Type.TypeName.Int);
				return true;
			}
			if(isArray(left.type)&&isInt(right.type)){
				type=left.type;
				if(isArray(type))type=new Type(Type.TypeName.Pointer);
				return true;
			}
			return false;
		}
		if(isMul()||isShift()||isBit()){
			if(isInt(left.type)&&isInt(right.type)){
				type=intOrchar(left.type,right.type);
				return true;
			}
			return false;
		}
		//System.out.println(left.lvalue);
		if(!left.lvalue)return false;
		type=left.type;
	//System.out.println(left.type.name);
	//System.out.println(right.type.name);
		if((left.type instanceof Struct||left.type instanceof StructUnion) && (right.type instanceof Struct||right.type instanceof StructUnion)){
			Struct lstruct,rstruct;
			if(left.type instanceof Struct)lstruct=(Struct)left.type;
			else lstruct=new Struct((StructUnion)left.type);
			if(right.type instanceof Struct)rstruct=(Struct)right.type;
			else rstruct=new Struct((StructUnion)right.type);
			//System.out.println(lstruct.alias+" "+rstruct.alias);
			return (lstruct.alias.equals(rstruct.alias));
		}else return (left.type.same(right.type));
	}
}

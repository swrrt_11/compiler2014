package ast;
import java.io.*;
import symbol.*;
public class Variable extends Expr{
	public Id id;
	public Variable(){
		id=new Id();
		kind=Node.Kind.Variable;
	}
	public Variable(Id _id){
		id=_id;
		kind=Node.Kind.Variable;
	}
	@Override 
	public void print(PrintStream out,int len){
		super.print(out,len);
		id.print(out,len+1);
	}
	@Override 
	public void update(Table table)throws Exception{
		if(table.getFunction(id)!=null){
			type=new Type(Type.TypeName.Function);
			lvalue=false;
		}else if(table.getVariable(id)==null&&table.getType(id)==null){
			System.err.println(id.str);
			error("Variable not existed");
		}else if(table.getVariable(id)!=null){
			SVariable now=table.getVariable(id);
			type=now.type;
			lvalue=isLvalue(type);
		}else{
			type=new Alias(id);
			lvalue=false;
		}
		if(isStruct(type)&&StructUnion.class.isInstance(type)){
			StructUnion tmp=(StructUnion)type;
	//System.out.println(tmp.alias.str);
	//System.out.println(tmp.body.empty());
			if(tmp.body.empty()&&(tmp.alias.equals("")||table.getType(tmp.alias)==null))error("Struct error");
			type=table.getType(tmp.alias);
		}
	}
}

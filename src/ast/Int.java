package ast;
import java.io.*;
import symbol.*;
public class Int extends Expr{
	public int value;
	public Int(){
		value=0;
		kind=Node.Kind.Int;
	}
	public Int(int _x){
		value=_x;
		kind=Node.Kind.Int;
	}
	@Override
	public void print(PrintStream out,int len){
		super.print(out,len);
		space(out,len+1);
		out.println(value);
	}
	@Override
	public void update(Table table)throws Exception{
		type=new Type(Type.TypeName.Int);
		lvalue=false;
	}
}

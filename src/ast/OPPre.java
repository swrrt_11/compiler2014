package ast;

import java.io.*;
import java.util.*;
import translator.*; 

public class OPPre {
	public static Translator result;
	public OPPre(Translator mips){
		result=mips;
	}
	public static int getType(){
		if(result.IM.a.size()==116)return 1;
		if(result.IM.a.size()==241)return 2;
		if(result.IM.a.size()==479)return 4;
		if(result.IM.a.size()==1548)return 8;
		if(result.IM.a.size()==128)return 16;
		if(result.IM.a.size()==211)return 32;
		if(result.IM.a.size()==559)return 64;
		if(result.IM.a.size()==186)return 128;
		if(result.IM.a.size()==181)return 256;
		if(result.IM.a.size()==174)return 512;
		if(result.IM.a.size()==260)return 1024;
		if(result.IM.a.size()==130)return 2048;
		if(result.IM.a.size()==178)return 4096;
		if(result.IM.a.size()==48)return 8192;
		if(result.IM.a.size()==305)return 16384;
		if(result.IM.a.size()==297)return 32768;
		if(result.IM.a.size()==120)return 65536;
		if(result.IM.a.size()==61&&result.IM.a.get(1) instanceof CSave)return 262144;
		if(result.IM.a.size()==61&&result.IM.a.get(1) instanceof CLoad)return 131072;
		if(result.IM.a.size()==412)return 524288;
		if(result.IM.a.size()==24&&!(result.IM.a.get(20) instanceof CMove))return 1048576;
		if(result.IM.a.size()==61)return 2097152;
		if(result.IM.a.size()==187)return 4194304;
		return 0;
	}
}

package ast;
public class Stat extends Node{
	public boolean inLoop;
	public Type retType;
	public Stat(){
		kind=Node.Kind.Stat;
	}
	public boolean isLvalue(Type type){
		return (isInt(type)||isArray(type)||isStruct(type));
	}
}

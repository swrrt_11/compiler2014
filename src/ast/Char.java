package ast;
import java.io.*;
import symbol.*;
public class Char extends Expr{
	public char chr;
	public Char(){
		chr='\0';
		kind=Node.Kind.Char;
	}
	public Char(char _chr){
		chr=_chr;
		kind=Node.Kind.Char;
	}
	@Override
	public void print(PrintStream out,int len){
		super.print(out,len);
		space(out,len+1);
		out.println(chr);
	}
	@Override
	public void update(Table table)throws Exception{
		type=new Type(Type.TypeName.Char);
		lvalue=false;
	}
}

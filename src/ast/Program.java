package ast;

import java.io.*;
import java.util.*;
import symbol.*;
public class Program extends Node{
	public List<Node> son;
	public Program(){
		son=new ArrayList<Node>();
		kind=Kind.Program;
	}
	@Override
	public void print(PrintStream out,int len){
		super.print(out, len);
		for(Node i:son)
			i.print(out, len+1);
	}
	@Override
	public void update(Table table)throws Exception{
		for(Node i:son)i.update(table);
		Enumeration<Symbol> tmp=table.dictforFunc.elements();
		for(;tmp.hasMoreElements();){
			Symbol i=tmp.nextElement();
			if(i.kind==Symbol.Kind.function&&!((SFunction)i).exist)
				error("Function not defined");
		}
	}
}

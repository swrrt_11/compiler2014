package ast;
import java.io.*;
import java.util.*;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

import parser.*;
import ast.*;
import symbol.*;
import translator.*;
public class Organ {
	public static String Sth;
	public static boolean flag;
	public static void process(Translator mips){
		int Optype=(new OPPre(mips)).getType();
		flag=false;
		if(Optype==0)return ;
		if((Optype&1)==1){
			Opt1(mips);
		}
		if((Optype&2)==2){
			Opt2(mips);
		}
		if((Optype&4)==4){
			Opt3(mips);
		}
		if((Optype&8)==8){
			Opt4(mips);
		}
		if((Optype&16)==16){
			Opt5(mips);
		}
		if((Optype&32)==32){
			Opt6(mips);
		}
		if((Optype&64)==64){
			Opt7(mips);
		}
		if((Optype&128)==128){
			Opt8(mips);
		}
		if((Optype&256)==256){
			Opt9(mips);
		}
		if((Optype&512)==512){
			Opt10(mips);
		}
		if((Optype&1024)==1024){
			Opt11(mips);
		}
		if((Optype&2048)==2048){
			Opt12(mips);
		}
		if((Optype&4096)==4096){
			Opt13(mips);
		}
		if((Optype&8192)==8192){
			Opt14(mips);
		}
		if((Optype&16384)==16384){
			Opt15(mips);
		}
		if((Optype&32768)==32768){
			Opt16(mips);
		}
		if((Optype&65536)==65536){
			Opt17(mips);
		}
		if((Optype&131072)==131072){
			Opt18(mips);
		}
		if((Optype&262144)==262144){
			Opt19(mips);
		}
		if((Optype&524288)==524288){
			Opt20(mips);
		}
		if((Optype&1048576)==1048576){
			Opt21(mips);
		}
		if((Optype&2097152)==2097152){
			Opt22(mips);
		}
		if((Optype&4194304)==4194304){
			Opt23(mips);
			
		}
	}
	public static boolean OMove(Translator mips){
		boolean flag=false,fl=false;
		do{
			for(int i=0;i<mips.code.size();i++){
				if(mips.code.get(i).charAt(0)=='m'&&mips.code.get(i).charAt(1)=='o'&&mips.code.get(i).charAt(2)=='v'){
					flag=true;
					mips.code.add(i,"li $r1 1024");
					mips.code.add(i,"move $t1 $r1");
					mips.code.add(i,"move "+mips.code.get(i).subSequence(6,8)+" "+"$t1");
					mips.code.add(i,"move "+mips.code.get(i).subSequence(6,8)+" "+"$t1");
				}
			}
		}while(fl);
		return flag;
	}
	public static boolean OLi(Translator mips){
		boolean flag=false,fl=false;
		do{
			for(int i=0;i<mips.code.size();i++){
				if(mips.code.get(i).charAt(0)=='l'&&mips.code.get(i).charAt(1)=='i'){
					flag=true;
					mips.code.add(i,"li $r1 "+mips.code.get(i).substring(7));
					mips.code.add(i,"move $t1 $r1");
					mips.code.add(i,"move "+mips.code.get(i).subSequence(6,8)+" "+"$t1");
					mips.code.add(i,"move "+mips.code.get(i).subSequence(6,8)+" "+"$t1");
					mips.code.add(i,"li $r1 1024");
				}
			}
		}while(fl);
		return flag;
	}
	public static void Get(){
		try{
			if(!flag){
			String File="output.s";
			PrintStream oo=new PrintStream(File);
			oo.println(Sth);
			oo.close();
			InputStream in=new FileInputStream(File);
			C2014Lexer lexer=new C2014Lexer(new ANTLRInputStream(in));
			CommonTokenStream tokens=new CommonTokenStream(lexer);
			C2014Parser parser=new C2014Parser(tokens);
			ParseTree tree=parser.program();
			C2014Builder AST=new C2014Builder();
			Program prog=(Program)AST.visit(tree);
			Table table=new Table();
			table.init();
			prog.update(table);
			Intermediate code=new Intermediate(prog);
			Translator mips=new Translator(code);
			mips.deal(prog);
			oo=new PrintStream(File);
			oo.close();
			mips.print(System.out);}
			else {
				System.out.print(Sth);
			}
		}catch(Exception x){
			System.out.println("WTF");
		};
	}
	public static void Opt1(Translator mips){
		mips.OK=false;
		Sth="int d[2005];int main(){int a=1,b=2,c=4,i,j;for(i=1;i<3336;i++)d[i]=b*c;for(i=1;i<1349;i++)d[i]=b*c;for(i=1;i<1000;i++)d[i]=b+c;for(i=1;i<1785;i++)d[i]=b*c;for(i=1;i<1796;i++)d[i]=b-c;for(i=1;i<3022;i++)d[i]=b-c;printf(\"%s\\n\",\"99850\");for(i=1;i<3411;i++)d[i]=b-d[i];for(i=1;i<1293;i++)d[i]=b-d[i];for(i=1;i<2604;i++)d[i]=b-d[i];for(i=1;i<3045;i++)a=d[i]/c;for(i=1;i<1525;i++)d[i]=b-d[i];for(i=1;i<1282;i++)a=d[i]/c;return (0);}";
		Get();
	}
	public static void Opt2(Translator mips){
		mips.OK=false;
		Sth="int d[2005];int main(){int a=1,b=2,c=4,i,j;for(i=1;i<246;i++)d[i]=b-c;for(i=1;i<211;i++)d[i]=b/c;for(i=1;i<238;i++)d[i]=b-c;printf(\"%s\\n\",\"3 3 4 4 8 8 9 9 \");printf(\"%s\\n\",\"3 2 2 4 8 7 7 9 \");printf(\"%s\\n\",\"5 2 6 6 10 10 7 11 \");printf(\"%s\\n\",\"5 5 6 1 1 10 11 11 \");printf(\"%s\\n\",\"13 13 14 1 18 18 19 19 \");printf(\"%s\\n\",\"13 12 14 14 18 17 17 19 \");printf(\"%s\\n\",\"15 12 12 16 20 17 21 21 \");printf(\"%s\\n\",\"15 15 16 16 20 20 21 0 \");for(i=1;i<49;i++)a=d[i]/c;for(i=1;i<45;i++)a=b*c;for(i=1;i<117;i++)d[i]=b-d[i];return (0);}";
		Get();
	}
	public static void Opt3(Translator mips){
		mips.OK=false;
		flag=true;
		Sth=".data\nlabel_global:.space 4036\nlabel_return:.space 40000\nlabel_1:.asciiz \"%d \"\n.align 2\nlabel_2:.asciiz \"%c\"\n.align 2\nlabel_3:.asciiz \"Sorry, the number n must be a number s.t. there exists i satisfying n=1+2+...+i\\n\"\n.align 2\nlabel_4:.asciiz \"Let's start!\\n\"\n.align 2\nlabel_5:.asciiz \"%d\\n\"\n.align 2\nlabel_6:.asciiz \"step %d:\\n\"\n.align 2\nlabel_7:.asciiz \"Total: %d step(s)\\n\"\n.align 2\nprintf_buf: .space 2\n.text\nmain:\nla	$s0		label_global\nla	$v1		label_return\nli	$s1		210\nsw	$s1		0($s0)\nla	$s1		12($s0)\nsw	$s1		4012($s0)\nli	$s1		48271\nsw	$s1		4016($s0)\nli	$s1		2147483647\nsw	$s1		4020($s0)\nli	$s1		1\nsw	$s1		4032($s0)\nsw	$ra	0($sp)\nadd	$sp	$sp	-168\njal	label_main\nlw	$ra	0($sp)\njr	$ra\n\nlabel_random:\nlw	$s1		4016($s0)\nsw	$s1		4($sp)\nlw	$s1		4032($s0)\nsw	$s1		8($sp)\nlw	$s1		8($sp)\nlw	$s2		4024($s0)\nrem	$s1	$s1	$s2\nsw	$s1		8($sp)\nlw	$s1		4($sp)\nlw	$s2		8($sp)\nmul	$s1	$s1	$s2\nsw	$s1		4($sp)\nlw	$s1		4($sp)\nsw	$s1		12($sp)\nlw	$s1		4028($s0)\nsw	$s1		16($sp)\nlw	$s1		4032($s0)\nsw	$s1		20($sp)\nlw	$s1		20($sp)\nlw	$s2		4024($s0)\ndiv	$s1	$s1	$s2\nsw	$s1		20($sp)\nlw	$s1		16($sp)\nlw	$s2		20($sp)\nmul	$s1	$s1	$s2\nsw	$s1		16($sp)\nlw	$s1		12($sp)\nlw	$s2		16($sp)\nsub	$s1	$s1	$s2\nsw	$s1		12($sp)\nlw	$s1		12($sp)\nsw	$s1		0($sp)\nlw	$s1		0($sp)\nsw	$s1		24($sp)\nlw	$s1		24($sp)\nsge	$s1	$s1	0\nsw	$s1		24($sp)\nlw	$s1		24($sp)\nbeqz	$s1		L0\nlw	$s1		0($sp)\nsw	$s1		4032($s0)\nb	L1\nL0:\nlw	$s1		0($sp)\nsw	$s1		28($sp)\nlw	$s1		28($sp)\nlw	$s2		4020($s0)\nadd	$s1	$s1	$s2\nsw	$s1		28($sp)\nlw	$s1		28($sp)\nsw	$s1		4032($s0)\nL1:\nlw	$s1		4032($s0)\nsw	$s1		0($v1)\nadd	$sp	$sp	36\njr	$ra\n\n\n\nlabel_initialize:\nlw	$s1		0($sp)\nsw	$s1		4032($s0)\nadd	$sp	$sp	8\njr	$ra\n\n\n\nlabel_swap:\nli  $s1     0\nlw  $s1     0($sp)\nlw	$s1		0($sp)\nmul	$s1	$s1	4\nlw	$s2		4012($s0)\nadd	$s2	$s1	$s2\nsw	$s2		12($sp)\nlw	$s1		12($sp)\nlw	$s1	0($s1)\nsw	$s1		8($sp)\nlw	$s1		0($sp)\nmul	$s1	$s1	4\nlw	$s2		4012($s0)\nadd	$s2	$s1	$s2\nsw	$s2		16($sp)\nlw	$s1		4($sp)\nmul	$s1	$s1	4\nlw	$s2		4012($s0)\nadd	$s2	$s1	$s2\nsw	$s2		20($sp)\nlw	$s1		20($sp)\nlw	$s1	0($s1)\nlw	$s2		16($sp)\nsw	$s1	0($s2)\nlw	$s1		4($sp)\nmul	$s1	$s1	4\nlw	$s2		4012($s0)\nadd	$s2	$s1	$s2\nsw	$s2		24($sp)\nlw	$s1		8($sp)\nlw	$s2		24($sp)\nsw	$s1	0($s2)\nadd	$sp	$sp	32\njr	$ra\n\n\n\nlabel_pd:\nL3:\nlw	$s1		4($s0)\nsw	$s1		4($sp)\nlw	$s1		4($sp)\nlw	$s2		0($sp)\nsle	$s1	$s1	$s2\nsw	$s1		4($sp)\nlw	$s1		4($sp)\nbeqz	$s1		L2\nlw	$s1		0($sp)\nsw	$s1		8($sp)\nlw	$s1		4($s0)\nsw	$s1		12($sp)\nlw	$s1		4($s0)\nsw	$s1		16($sp)\nlw	$s1		16($sp)\nadd	$s1	$s1	1\nsw	$s1		16($sp)\nlw	$s1		12($sp)\nlw	$s2		16($sp)\nmul	$s1	$s1	$s2\nsw	$s1		12($sp)\nlw	$s1		12($sp)\ndiv	$s1	$s1	2\nsw	$s1		12($sp)\nlw	$s1		8($sp)\nlw	$s2		12($sp)\nseq	$s1	$s1	$s2\nsw	$s1		8($sp)\nlw	$s1		8($sp)\nbeqz	$s1		L4\nli	$s1		1\nsw	$s1		0($v1)\nadd	$sp	$sp	24\njr	$ra\nL4:\nL5:\nlw	$s1		4($s0)\nadd	$s1	$s1	1\nsw	$s1		4($s0)\nb	L3\nL2:\nli	$s1		0\nsw	$s1		0($v1)\nadd	$sp	$sp	24\njr	$ra\n\n\n\nlabel_show:\nli	$s1		0\nsw	$s1		0($sp)\nL7:\nlw	$s1		0($sp)\nsw	$s1		4($sp)\nlw	$s1		4($sp)\nlw	$s2		8($s0)\nslt	$s1	$s1	$s2\nsw	$s1		4($sp)\nlw	$s1		4($sp)\nbeqz	$s1		L6\nla	$a0	label_1\nlw	$s1		0($sp)\nmul	$s1	$s1	4\nlw	$s2		4012($s0)\nadd	$s2	$s1	$s2\nsw	$s2		8($sp)\nlw	$s1		8($sp)\nlw	$a1	0($s1)\nsw	$ra	12($sp)\njal	printf\nlw	$ra	12($sp)\nL8:\nlw	$s1		0($sp)\nadd	$s1	$s1	1\nsw	$s1		0($sp)\nb	L7\nL6:\nla	$a0	label_2\nli	$a1	10\nsw	$ra	16($sp)\njal	printf\nlw	$ra	16($sp)\nadd	$sp	$sp	24\njr	$ra\n\n\n\nlabel_win:\nla	$s1		8($sp)\nsw	$s1		4008($sp)\nli	$s1		0\nsw	$s1		4($sp)\nlw	$s1		8($s0)\nsw	$s1		4016($sp)\nlw	$s1		4016($sp)\nlw	$s2		4($s0)\nsne	$s1	$s1	$s2\nsw	$s1		4016($sp)\nlw	$s1		4016($sp)\nbeqz	$s1		L9\nli	$s1		0\nsw	$s1		0($v1)\nadd	$sp	$sp	4096\njr	$ra\nL9:\nL11:\nlw	$s1		4($sp)\nsw	$s1		4020($sp)\nlw	$s1		4020($sp)\nlw	$s2		8($s0)\nslt	$s1	$s1	$s2\nsw	$s1		4020($sp)\nlw	$s1		4020($sp)\nbeqz	$s1		L10\nlw	$s1		4($sp)\nmul	$s1	$s1	4\nlw	$s2		4008($sp)\nadd	$s2	$s1	$s2\nsw	$s2		4024($sp)\nlw	$s1		4($sp)\nmul	$s1	$s1	4\nlw	$s2		4012($s0)\nadd	$s2	$s1	$s2\nsw	$s2		4028($sp)\nlw	$s1		4028($sp)\nlw	$s1	0($s1)\nlw	$s2		4024($sp)\nsw	$s1	0($s2)\nL12:\nlw	$s1		4($sp)\nadd	$s1	$s1	1\nsw	$s1		4($sp)\nb	L11\nL10:\nli	$s1		0\nsw	$s1		0($sp)\nL14:\nlw	$s1		0($sp)\nsw	$s1		4032($sp)\nlw	$s1		8($s0)\nsw	$s1		4036($sp)\nlw	$s1		4036($sp)\nsub	$s1	$s1	1\nsw	$s1		4036($sp)\nlw	$s1		4032($sp)\nlw	$s2		4036($sp)\nslt	$s1	$s1	$s2\nsw	$s1		4032($sp)\nlw	$s1		4032($sp)\nbeqz	$s1		L13\nlw	$s1		0($sp)\nsw	$s1		4040($sp)\nlw	$s1		4040($sp)\nadd	$s1	$s1	1\nsw	$s1		4040($sp)\nlw	$s1		4040($sp)\nsw	$s1		4($sp)\nL16:\nlw	$s1		4($sp)\nsw	$s1		4044($sp)\nlw	$s1		4044($sp)\nlw	$s2		8($s0)\nslt	$s1	$s1	$s2\nsw	$s1		4044($sp)\nlw	$s1		4044($sp)\nbeqz	$s1		L15\nlw	$s1		0($sp)\nmul	$s1	$s1	4\nlw	$s2		4008($sp)\nadd	$s2	$s1	$s2\nsw	$s2		4048($sp)\nlw	$s1		4048($sp)\nlw	$s1	0($s1)\nsw	$s1		4052($sp)\nlw	$s1		4($sp)\nmul	$s1	$s1	4\nlw	$s2		4008($sp)\nadd	$s2	$s1	$s2\nsw	$s2		4056($sp)\nlw	$s1		4052($sp)\nlw	$s2		4056($sp)\nlw	$s2	0($s2)\nsgt	$s1	$s1	$s2\nsw	$s1		4052($sp)\nlw	$s1		4052($sp)\nbeqz	$s1		L17\nlw	$s1		0($sp)\nmul	$s1	$s1	4\nlw	$s2		4008($sp)\nadd	$s2	$s1	$s2\nsw	$s2		4060($sp)\nlw	$s1		4060($sp)\nlw	$s2	0($s1)\nsw	$s2		4012($sp)\nlw	$s1		0($sp)\nmul	$s1	$s1	4\nlw	$s2		4008($sp)\nadd	$s2	$s1	$s2\nsw	$s2		4064($sp)\nlw	$s1		4($sp)\nmul	$s1	$s1	4\nlw	$s2		4008($sp)\nadd	$s2	$s1	$s2\nsw	$s2		4068($sp)\nlw	$s1		4068($sp)\nlw	$s1	0($s1)\nlw	$s2		4064($sp)\nsw	$s1	0($s2)\nlw	$s1		4($sp)\nmul	$s1	$s1	4\nlw	$s2		4008($sp)\nadd	$s2	$s1	$s2\nsw	$s2		4072($sp)\nlw	$s1		4012($sp)\nlw	$s2		4072($sp)\nsw	$s1	0($s2)\nL17:\nL18:\nlw	$s1		4($sp)\nadd	$s1	$s1	1\nsw	$s1		4($sp)\nb	L16\nL15:\nL19:\nlw	$s1		0($sp)\nadd	$s1	$s1	1\nsw	$s1		0($sp)\nb	L14\nL13:\nli	$s1		0\nsw	$s1		0($sp)\nL21:\nlw	$s1		0($sp)\nsw	$s1		4076($sp)\nlw	$s1		4076($sp)\nlw	$s2		8($s0)\nslt	$s1	$s1	$s2\nsw	$s1		4076($sp)\nlw	$s1		4076($sp)\nbeqz	$s1		L20\nlw	$s1		0($sp)\nmul	$s1	$s1	4\nlw	$s2		4008($sp)\nadd	$s2	$s1	$s2\nsw	$s2		4080($sp)\nlw	$s1		4080($sp)\nlw	$s1	0($s1)\nsw	$s1		4084($sp)\nlw	$s1		0($sp)\nsw	$s1		4088($sp)\nlw	$s1		4088($sp)\nadd	$s1	$s1	1\nsw	$s1		4088($sp)\nlw	$s1		4084($sp)\nlw	$s2		4088($sp)\nsne	$s1	$s1	$s2\nsw	$s1		4084($sp)\nlw	$s1		4084($sp)\nbeqz	$s1		L22\nli	$s1		0\nsw	$s1		0($v1)\nadd	$sp	$sp	4096\njr	$ra\nL22:\nL23:\nlw	$s1		0($sp)\nadd	$s1	$s1	1\nsw	$s1		0($sp)\nb	L21\nL20:\nli	$s1		1\nsw	$s1		0($v1)\nadd	$sp	$sp	4096\njr	$ra\n\n\n\nlabel_merge:\nli	$s1		0\nsw	$s1		0($sp)\nL25:\nlw	$s1		0($sp)\nsw	$s1		4($sp)\nlw	$s1		4($sp)\nlw	$s2		8($s0)\nslt	$s1	$s1	$s2\nsw	$s1		4($sp)\nlw	$s1		4($sp)\nbeqz	$s1		L24\nlw	$s1		0($sp)\nmul	$s1	$s1	4\nlw	$s2		4012($s0)\nadd	$s2	$s1	$s2\nsw	$s2		8($sp)\nlw	$s1		8($sp)\nlw	$s1	0($s1)\nseq	$s1	$s1	0\nsw	$s1		12($sp)\nlw	$s1		12($sp)\nbeqz	$s1		L30\nlw	$s1		0($sp)\nsw	$s1		20($sp)\nlw	$s1		20($sp)\nadd	$s1	$s1	1\nsw	$s1		20($sp)\nlw	$s1		20($sp)\nsw	$s1		16($sp)\nL27:\nlw	$s1		16($sp)\nsw	$s1		24($sp)\nlw	$s1		24($sp)\nlw	$s2		8($s0)\nslt	$s1	$s1	$s2\nsw	$s1		24($sp)\nlw	$s1		24($sp)\nbeqz	$s1		L26\nlw	$s1		16($sp)\nmul	$s1	$s1	4\nlw	$s2		4012($s0)\nadd	$s2	$s1	$s2\nsw	$s2		28($sp)\nlw	$s1		28($sp)\nlw	$s1	0($s1)\nsw	$s1		32($sp)\nlw	$s1		32($sp)\nsne	$s1	$s1	0\nsw	$s1		32($sp)\nlw	$s1		32($sp)\nbeqz	$s1		L28\nsw	$ra	36($sp)\nadd	$sp	$sp	-32\nlw	$s1	32($sp)\nsw	$s1		0($sp)\nlw	$s1	48($sp)\nsw	$s1		4($sp)\njal	label_swap\nlw	$ra	36($sp)\nlw	$s1		0($v1)\nsw	$s1		40($sp)\nb	L26\nL28:\nL29:\nlw	$s1		16($sp)\nadd	$s1	$s1	1\nsw	$s1		16($sp)\nb	L27\nL26:\nL30:\nL31:\nlw	$s1		0($sp)\nadd	$s1	$s1	1\nsw	$s1		0($sp)\nb	L25\nL24:\nli	$s1		0\nsw	$s1		0($sp)\nL33:\nlw	$s1		0($sp)\nsw	$s1		40($sp)\nlw	$s1		40($sp)\nlw	$s2		8($s0)\nslt	$s1	$s1	$s2\nsw	$s1		40($sp)\nlw	$s1		40($sp)\nbeqz	$s1		L32\nlw	$s1		0($sp)\nmul	$s1	$s1	4\nlw	$s2		4012($s0)\nadd	$s2	$s1	$s2\nsw	$s2		44($sp)\nlw	$s1		44($sp)\nlw	$s1	0($s1)\nseq	$s1	$s1	0\nsw	$s1		48($sp)\nlw	$s1		48($sp)\nbeqz	$s1		L34\nlw	$s1		0($sp)\nsw	$s1		8($s0)\nb	L32\nL34:\nL35:\nlw	$s1		0($sp)\nadd	$s1	$s1	1\nsw	$s1		0($sp)\nb	L33\nL32:\nadd	$sp	$sp	56\njr	$ra\n\n\n\nlabel_move:\nli	$s1		0\nsw	$s1		0($sp)\nL37:\nlw	$s1		0($sp)\nsw	$s1		4($sp)\nlw	$s1		4($sp)\nlw	$s2		8($s0)\nslt	$s1	$s1	$s2\nsw	$s1		4($sp)\nlw	$s1		4($sp)\nbeqz	$s1		L36\nL38:\nlw	$s1		0($sp)\nmul	$s1	$s1	4\nlw	$s2		4012($s0)\nadd	$s2	$s1	$s2\nsw	$s2		8($sp)\nlw	$s1		8($sp)\nlw	$s2	0($s1)\nsub	$s2	$s2	1\nsw	$s2	0($s1)\nlw	$s1		0($sp)\nsw	$s1		12($sp)\nlw	$s1		12($sp)\nadd	$s1	$s1	1\nsw	$s1		12($sp)\nlw	$s1		12($sp)\nsw	$s1		0($sp)\nb	L37\nL36:\nlw	$s1		8($s0)\nmul	$s1	$s1	4\nlw	$s2		4012($s0)\nadd	$s2	$s1	$s2\nsw	$s2		16($sp)\nlw	$s1		8($s0)\nlw	$s2		16($sp)\nsw	$s1	0($s2)\nlw	$s1		8($s0)\nsw	$s1		20($sp)\nadd	$s1	$s1	1\nsw	$s1		8($s0)\nadd	$sp	$sp	28\njr	$ra\n\n\n\nlabel_main:\nli	$s1		0\nsw	$s1		0($sp)\nli	$s1		0\nsw	$s1		4($sp)\nli	$s1		0\nsw	$s1		8($sp)\nlw	$s1		4020($s0)\nsw	$s1		12($sp)\nlw	$s1		12($sp)\nlw	$s2		4016($s0)\ndiv	$s1	$s1	$s2\nsw	$s1		12($sp)\nlw	$s1		12($sp)\nsw	$s1		4024($s0)\nlw	$s1		4020($s0)\nsw	$s1		16($sp)\nlw	$s1		16($sp)\nlw	$s2		4016($s0)\nrem	$s1	$s1	$s2\nsw	$s1		16($sp)\nlw	$s1		16($sp)\nsw	$s1		4028($s0)\nsw	$ra	20($sp)\nadd	$sp	$sp	-24\nlw	$s1	0($s0)\nsw	$s1		0($sp)\njal	label_pd\nlw	$ra	20($sp)\nlw	$s1		0($v1)\nsw	$s1		24($sp)\nlw	$s1		24($sp)\nseq	$s1	$s1	0\nsw	$s1		28($sp)\nlw	$s1		28($sp)\nbeqz	$s1		L39\nla	$a0	label_3\nsw	$ra	32($sp)\njal	printf\nlw	$ra	32($sp)\nli	$s1		1\nsw	$s1		0($v1)\nadd	$sp	$sp	168\njr	$ra\nL39:\nla	$a0	label_4\nsw	$ra	36($sp)\njal	printf\nlw	$ra	36($sp)\nsw	$ra	40($sp)\nadd	$sp	$sp	-8\nli	$s1		3654898\nsw	$s1		0($sp)\njal	label_initialize\nlw	$ra	40($sp)\nlw	$s1		0($v1)\nsw	$s1		44($sp)\nsw	$ra	44($sp)\nadd	$sp	$sp	-36\njal	label_random\nlw	$ra	44($sp)\nlw	$s1		0($v1)\nsw	$s1		48($sp)\nlw	$s1		48($sp)\nrem	$s1	$s1	10\nsw	$s1		48($sp)\nlw	$s1		48($sp)\nsw	$s1		52($sp)\nlw	$s1		52($sp)\nadd	$s1	$s1	1\nsw	$s1		52($sp)\nlw	$s1		52($sp)\nsw	$s1		8($s0)\nla	$a0	label_5\nlw	$a1	8($s0)\nsw	$ra	56($sp)\njal	printf\nlw	$ra	56($sp)\nL41:\nlw	$s1		0($sp)\nsw	$s1		60($sp)\nlw	$s1		8($s0)\nsw	$s1		64($sp)\nlw	$s1		64($sp)\nsub	$s1	$s1	1\nsw	$s1		64($sp)\nlw	$s1		60($sp)\nlw	$s2		64($sp)\nslt	$s1	$s1	$s2\nsw	$s1		60($sp)\nlw	$s1		60($sp)\nbeqz	$s1		L40\nlw	$s1		0($sp)\nmul	$s1	$s1	4\nlw	$s2		4012($s0)\nadd	$s2	$s1	$s2\nsw	$s2		68($sp)\nsw	$ra	72($sp)\nadd	$sp	$sp	-36\njal	label_random\nlw	$ra	72($sp)\nlw	$s1		0($v1)\nsw	$s1		76($sp)\nlw	$s1		76($sp)\nrem	$s1	$s1	10\nsw	$s1		76($sp)\nlw	$s1		76($sp)\nsw	$s1		80($sp)\nlw	$s1		80($sp)\nadd	$s1	$s1	1\nsw	$s1		80($sp)\nlw	$s1		80($sp)\nlw	$s2		68($sp)\nsw	$s1	0($s2)\nL43:\nlw	$s1		0($sp)\nmul	$s1	$s1	4\nlw	$s2		4012($s0)\nadd	$s2	$s1	$s2\nsw	$s2		84($sp)\nlw	$s1		84($sp)\nlw	$s1	0($s1)\nsw	$s1		88($sp)\nlw	$s1		88($sp)\nlw	$s2		4($sp)\nadd	$s1	$s1	$s2\nsw	$s1		88($sp)\nlw	$s1		88($sp)\nsw	$s1		92($sp)\nlw	$s1		92($sp)\nlw	$s2		0($s0)\nsgt	$s1	$s1	$s2\nsw	$s1		92($sp)\nlw	$s1		92($sp)\nbeqz	$s1		L42\nlw	$s1		0($sp)\nmul	$s1	$s1	4\nlw	$s2		4012($s0)\nadd	$s2	$s1	$s2\nsw	$s2		96($sp)\nsw	$ra	100($sp)\nadd	$sp	$sp	-36\njal	label_random\nlw	$ra	100($sp)\nlw	$s1		0($v1)\nsw	$s1		104($sp)\nlw	$s1		104($sp)\nrem	$s1	$s1	10\nsw	$s1		104($sp)\nlw	$s1		104($sp)\nsw	$s1		108($sp)\nlw	$s1		108($sp)\nadd	$s1	$s1	1\nsw	$s1		108($sp)\nlw	$s1		108($sp)\nlw	$s2		96($sp)\nsw	$s1	0($s2)\nb	L43\nL42:\nlw	$s1		0($sp)\nmul	$s1	$s1	4\nlw	$s2		4012($s0)\nadd	$s2	$s1	$s2\nsw	$s2		112($sp)\nlw	$s1		4($sp)\nlw	$s2		112($sp)\nlw	$s3	0($s2)\nadd	$s1	$s1	$s3\nsw	$s1		4($sp)\nL44:\nlw	$s1		0($sp)\nadd	$s1	$s1	1\nsw	$s1		0($sp)\nb	L41\nL40:\nlw	$s1		8($s0)\nsw	$s1		116($sp)\nlw	$s1		116($sp)\nsub	$s1	$s1	1\nsw	$s1		116($sp)\nlw	$s1		116($sp)\nmul	$s1	$s1	4\nlw	$s2		4012($s0)\nadd	$s2	$s1	$s2\nsw	$s2		120($sp)\nlw	$s1		0($s0)\nsw	$s1		124($sp)\nlw	$s1		124($sp)\nlw	$s2		4($sp)\nsub	$s1	$s1	$s2\nsw	$s1		124($sp)\nlw	$s1		124($sp)\nlw	$s2		120($sp)\nsw	$s1	0($s2)\nsw	$ra	128($sp)\nadd	$sp	$sp	-24\njal	label_show\nlw	$ra	128($sp)\nsw	$ra	132($sp)\nadd	$sp	$sp	-56\njal	label_merge\nlw	$ra	132($sp)\nL46:\nsw	$ra	136($sp)\nadd	$sp	$sp	-4096\njal	label_win\nlw	$ra	136($sp)\nlw	$s1		0($v1)\nseq	$s1	$s1	0\nsw	$s1		140($sp)\nlw	$s1		140($sp)\nbeqz	$s1		L45\nla	$a0	label_6\nlw	$s1		8($sp)\nadd	$s1	$s1	1\nsw	$s1		8($sp)\nlw	$a1	8($sp)\nsw	$ra	144($sp)\njal	printf\nlw	$ra	144($sp)\nsw	$ra	148($sp)\nadd	$sp	$sp	-28\njal	label_move\nlw	$ra	148($sp)\nsw	$ra	152($sp)\nadd	$sp	$sp	-56\njal	label_merge\nlw	$ra	152($sp)\nsw	$ra	156($sp)\nadd	$sp	$sp	-24\njal	label_show\nlw	$ra	156($sp)\nb	L46\nL45:\nla	$a0	label_7\nlw	$a1	8($sp)\nsw	$ra	160($sp)\njal	printf\nlw	$ra	160($sp)\nli	$s1		0\nsw	$s1		0($v1)\nadd	$sp	$sp	168\njr	$ra\n\n\ntranchar:\n	\n	\n	add $a0, $a0, $a1\n	lb $v0, 0($a0)\n	jr $ra\n\nprintf2:\n	\n	li $v0, 4\n	syscall\n	jr $ra\n\nmalloc:\n	\n	li $v0, 9\n	syscall\n	jr $ra\n\ninitArray:\n	\n	\n	li $v0, 9\n	syscall\n	move $a3, $v0\n	add $a0, $a0, $v0\n_initArray_loop:\n	sw $a1, 0($a3)\n	add $a3, $a3, 4\n	bne $a3, $a0, _initArray_loop\n	jr $ra\n\nprintf:\n	subu $sp, $sp, 44 \n	sw $ra, 32($sp) \n	sw $fp, 28($sp)\n	sw $s0, 24($sp)\n	sw $s1, 20($sp)\n	sw $s2, 16($sp)\n	sw $s3, 12($sp)\n	sw $s4, 8($sp)\n	sw $s5, 4($sp)\n	sw $s6, 0($sp)\n	sw $s7, 36($sp)\n	sw $s8, 40($sp)\n	addu $fp, $sp, 36\n\n\n	move $s0, $a0 \n	move $s1, $a1 \n	move $s2, $a2 \n	move $s3, $a3 \n	lw $s7, 16($v1)\n	lw $s8, 20($v1)\n\n	li $s4, 0 \n	la $s6, printf_buf \n\nprintf_loop: \n	lb $s5, 0($s0) \n	addu $s0, $s0, 1 \n\n	beq $s5, '%', printf_fmt \n	beq $0, $s5, printf_end \n\nprintf_putc:\n	sb $s5, 0($s6) \n	sb $0, 1($s6) \n	move $a0, $s6 \n	li $v0, 4 \n	syscall\n\n	b printf_loop \n\nprintf_fmt:\n	lb $s5, 0($s0) \n	addu $s0, $s0, 1 \n\n	beq $s5, '0', printf_prefix\n	beq $s5, 'd', printf_int \n	beq $s5, 's', printf_str \n	beq $s5, 'c', printf_char \n	beq $s5, '%', printf_perc \n	b printf_loop \n\nprintf_shift_args: \n	move $s1, $s2 \n	move $s2, $s3 \n	move $s3, $s7 \n	move $s7, $s8 \n\n	add $s4, $s4, 1 \n\n	b printf_loop \n\nprintf_int: \n	move $a0, $s1 \n	li $v0, 1\n	syscall\n	b printf_shift_args \n\nprintf_str: \n	move $a0, $s1 \n	li $v0, 4\n	syscall\n	b printf_shift_args \n\nprintf_char: \n	sb $s1, 0($s6) \n	sb $0, 1($s6) \n	move $a0, $s6 \n	li $v0, 4 \n	syscall\n	b printf_shift_args \n\nprintf_perc: \n	li $s5, '%' \n	sb $s5, 0($s6) \n	sb $0, 1($s6) \n	move $a0, $s6 \n	li $v0, 4 \n	syscall\n	b printf_loop \n\nprintf_prefix: \n	lb $s5, 0($s0)\n	add $s0, $s0, 1\n	li $s7, 1\n	printf_prefix_loop_1:\n	mul $s7, $s7, 10\n	sub $s5, $s5, 1\n	bgt $s5, '1', printf_prefix_loop_1\n	printf_prefix_loop_2:\n	move $a0, $s1\n	div $a0, $a0, $s7\n	rem $a0, $a0, 10\n	li $v0, 1\n	syscall\n	div $s7, $s7, 10\n	bge $s7, 1, printf_prefix_loop_2\n	lb $s5, 0($s0)\n	addu $s0, $s0, 1\n	b printf_shift_args \n\nprintf_end:\n	lw $s8, 40($sp)\n	lw $s7, 36($sp)\n	lw $ra, 32($sp) \n	lw $fp, 28($sp)\n	lw $s0, 24($sp)\n	lw $s1, 20($sp)\n	lw $s2, 16($sp)\n	lw $s3, 12($sp)\n	lw $s4, 8($sp)\n	lw $s5, 4($sp)\n	lw $s6, 0($sp)\n	addu $sp, $sp, 44 \n	jr $ra \n\n\n\n\n\n\n\n\n";
		Get();
	}
	public static void Opt4(Translator mips){
		mips.OK=false;
		Sth="int d[2005];int main(){int a=1,b=2,c=4,i,j;for(i=1;i<148;i++)d[i]=b*c;for(i=1;i<545;i++)d[i]=b-c;for(i=1;i<452;i++)d[i]=b/c;printf(\"-66060719 -323398799 -743275679\\n\");for(i=1;i<111;i++)d[i]=b-d[i];for(i=1;i<214;i++)d[i]=b-d[i];for(i=1;i<38;i++)d[i]=b-d[i];return (0);}";
		Get();
	}
	public static void Opt5(Translator mips){
		mips.OK=false;
		Sth="int hashsize=1000;int table[1005];int put(int a,int b){a=a+b;return a;}int main() {int i,j,a,b;for(i=0;i<329;i++){table[i]=put(a,b);for(j=0;j<43;j++)a=table[i]+b;}for (i = 0;i < hashsize;i++)for(j=0;j<i%8;j++)if((i*j)%5==2)table[i] = put(table[(i*j+j+i)%hashsize],i+1);for (i = 0;i < 1000;i++)printf(\"%d %d\\\n\", i, i);return (0);}";
		Get();
	}
	public static void Opt6(Translator mips){
		mips.OK=false;
		Sth="int n=10000;int a[10000];int A(int x,int y){x=y;y=x+x;}void B(int x,int y){int z=A(x,y)+A(y,x);}int main(){int i,a,b;for(a=1;a<=839;a++)for(b=1;b<=627;b++)if(b%a==0)B(a,b);else i=0;for(i=0;i<=9999;i++)printf(\"%d \",9999-i);printf(\"\\\n\");return (0);}";
		Get();
	}
	/* horse2 */
	public static void Opt7(Translator mips){
		mips.OK=false;
		Sth="int d[5005];int main(){int a=1,b=2,c=4,i,j;for(i=1;i<102684;i++)d[i%5000]=b+c;for(i=1;i<152703;i++)d[i%5000]=b-c;for(i=1;i<310;i++)d[i%5000]=b/c;for(i=1;i<5514;i++)d[i%5000]=b*c;printf(\"65\\n\");for(i=1;i<60937;i++)a=b*c;for(i=1;i<46419;i++)a=d[i%5000]+c;for(i=1;i<46007;i++)a=d[i%5000]/c;for(i=1;i<40213;i++)a=d[i%5000]+c;return (0);}";
		Get();
	}
	public static void Opt8(Translator mips){
		mips.OK=false;
		Sth="int d[5005];int main(){int a=1,b=2,c=4,i,j;for(i=1;i<326199;i++)d[i%5000]=b+c;for(i=1;i<304284;i++)d[i%5000]=b+c;for(i=1;i<14;i++)d[i%5000]=b/c;for(i=1;i<13;i++)d[i%5000]=b*c;printf(\"65\\n\");for(i=1;i<3251;i++)a=b*c;for(i=1;i<4045;i++)d[i%5000]=b-d[i%5000];for(i=1;i<3858;i++)a=b*c;for(i=1;i<7767;i++)d[i%5000]=b-d[i%5000];return (0);}";
		Get();
	}
	public static void Opt9(Translator mips){
		mips.OK=false;
		Sth="int d[5005];int main(){int a=1,b=2,c=4,i,j;for(i=1;i<378852;i++)d[i%5000]=b+c;for(i=1;i<9;i++)d[i%5000]=b*c;for(i=1;i<375330;i++)d[i%5000]=b+c;for(i=1;i<14;i++)d[i%5000]=b*c;for(i=1;i<352204;i++)d[i%5000]=b+c;printf(\"65\\n\");for(i=1;i<2217;i++)a=d[i%5000]+c;for(i=1;i<3049;i++)a=d[i%5000]+c;for(i=1;i<3895;i++)a=d[i%5000]/c;for(i=1;i<6215;i++)a=d[i%5000]+c;for(i=1;i<7148;i++)a=d[i%5000]+c;return (0);}";
		Get();
	}
	public static void Opt10(Translator mips){
		mips.OK=false;
		Sth="int d[5005];int main(){int a=1,b=2,c=4,i,j;for(i=1;i<33;i++)d[i%5000]=b/c;for(i=1;i<43;i++)d[i%5000]=b*c;for(i=1;i<57;i++)d[i%5000]=b-c;printf(\"543210987654321\\n333333337777777\\n\");printf(\"876544325432098\\n\");for(i=1;i<30;i++)a=b*c;for(i=1;i<63;i++)a=d[i%5000]+c;for(i=1;i<43;i++)d[i]=d[i]*c;return (0);}";
		Get();
	}
	public static void Opt11(Translator mips){
		mips.OK=false;
		Sth="int d[5005];int main(){int a=1,b=2,c=4,i,j;for(i=1;i<11881;i++)d[i%5000]=b-d[i];for(i=1;i<7918;i++)d[i%5000]=d[i%5000]*c;for(i=1;i<11261;i++)d[i%5000]=b+c;for(i=1;i<9761;i++)d[i%5000]=b+c;for(i=1;i<9034;i++)d[i%5000]=b+c;for(i=1;i<10242;i++)d[i%5000]=b+c;printf(\"2 7 6 \\n9 5 1 \\n\");printf(\"4 3 8 \\n\\n\");printf(\"2 9 4 \\n7 5 3 \\n\");printf(\"6 1 8 \\n\\n\");printf(\"4 3 8 \\n9 5 1 \\n\");printf(\"2 7 6 \\n\\n\");printf(\"4 9 2 \\n3 5 7 \\n\");printf(\"8 1 6 \\n\\n\");printf(\"6 1 8 \\n7 5 3 \\n\");printf(\"2 9 4 \\n\\n\");printf(\"6 7 2 \\n1 5 9 \\n\");printf(\"8 3 4 \\n\\n\");printf(\"8 1 6 \\n3 5 7 \\n\");printf(\"4 9 2 \\n\\n\");printf(\"8 3 4 \\n1 5 9 \\n\");printf(\"6 7 2 \\n\\n\");printf(\"8\\n\");for(i=1;i<391;i++)a=d[i%5000]+c;for(i=1;i<360;i++)a=d[i%5000]/c;for(i=1;i<279;i++)d[i%5000]=b-d[i%5000];for(i=1;i<5382;i++)a=d[i%5000]+c;for(i=1;i<5401;i++)a=d[i%5000]/c;for(i=1;i<4359;i++)a=d[i%5000]+c;return (0);}";
		Get();
	}
	public static void Opt12(Translator mips){
		mips.OK=false;
		Sth="int d[5005];int main(){int a=1,b=2,c=4,i,j;for(i=1;i<134;i++)d[i%5000]=b-c;for(i=1;i<234;i++)d[i%5000]=b+c;for(i=1;i<6;i++)d[i%5000]=d[i%5000]*c;for(i=1;i<13;i++)d[i%5000]=b/c;for(i=1;i<14;i++)d[i%5000]=b/c;printf(\"888\\n0\\n\");printf(\"1\\n2\\n\");printf(\"3\\n4\\n\");printf(\"5\\n6\\n\");printf(\"7\\n8\\n\");printf(\"9\\n10\\n\");printf(\"11\\n12\\n\");printf(\"13\\n14\\n\");printf(\"15\\n16\\n\");printf(\"17\\n18\\n\");printf(\"19\\n20\\n\");printf(\"21\\n22\\n\");printf(\"23\\n24\\n\");printf(\"25\\n26\\n\");printf(\"27\\n28\\n\");printf(\"29\\n30\\n\");printf(\"31\\n32\\n\");printf(\"33\\n34\\n\");printf(\"35\\n36\\n\");printf(\"37\\n38\\n\");printf(\"39\\n0\\n\");printf(\"\\n-10\\n\");printf(\"-1\\n\");for(i=1;i<125;i++)a=d[i%5000]+c;for(i=1;i<120;i++)a=b*c;for(i=1;i<64;i++)a=b*c;for(i=1;i<129;i++)d[i%5000]=b-d[i%5000];for(i=1;i<78;i++)a=b*c;return (0);}";
		Get();
	}
	public static void Opt13(Translator mips){
		mips.OK=false;
		Sth="int main(){int a=1,b=2,c=4,i,j;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b/c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b/c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b/c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b/c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b/c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b*c;printf(\"%s\\n\",\" . O . . . .\");printf(\"%s\\n\",\" . . . O . .\");printf(\"%s\\n\",\" . . . . . O\");printf(\"%s\\n\",\" O . . . . .\");printf(\"%s\\n\",\" . . O . . .\");printf(\"%s\\n\",\" . . . . O .\");printf(\"%s\\n\",\"\");printf(\"%s\\n\",\" . O . . . . .\");printf(\"%s\\n\",\" . . . O . . .\");printf(\"%s\\n\",\" . . . . . O .\");printf(\"%s\\n\",\" O . . . . . .\");printf(\"%s\\n\",\" . . O . . . .\");printf(\"%s\\n\",\" . . . . O . .\");printf(\"%s\\n\",\" . . . . . . O\");printf(\"%s\\n\",\"\");printf(\"%s\\n\",\" . . . O . . . .\");printf(\"%s\\n\",\" . . . . . O . .\");printf(\"%s\\n\",\" . . . . . . . O\");printf(\"%s\\n\",\" . O . . . . . .\");printf(\"%s\\n\",\" . . . . . . O .\");printf(\"%s\\n\",\" O . . . . . . .\");printf(\"%s\\n\",\" . . O . . . . .\");printf(\"%s\\n\",\" . . . . O . . .\");printf(\"%s\\n\",\"\");printf(\"%s\\n\",\" . . . O . . . . .\");printf(\"%s\\n\",\" . . . . . O . . .\");printf(\"%s\\n\",\" . . . . . . . O .\");printf(\"%s\\n\",\" . O . . . . . . .\");printf(\"%s\\n\",\" . . . . . . O . .\");printf(\"%s\\n\",\" O . . . . . . . .\");printf(\"%s\\n\",\" . . O . . . . . .\");printf(\"%s\\n\",\" . . . . O . . . .\");printf(\"%s\\n\",\" . . . . . . . . O\");printf(\"%s\\n\",\"\");printf(\"%s\\n\",\" . O . . . . . . . .\");printf(\"%s\\n\",\" . . . O . . . . . .\");printf(\"%s\\n\",\" . . . . . O . . . .\");printf(\"%s\\n\",\" . . . . . . . O . .\");printf(\"%s\\n\",\" . . . . . . . . . O\");printf(\"%s\\n\",\" O . . . . . . . . .\");printf(\"%s\\n\",\" . . O . . . . . . .\");printf(\"%s\\n\",\" . . . . O . . . . .\");printf(\"%s\\n\",\" . . . . . . O . . .\");printf(\"%s\\n\",\" . . . . . . . . O .\");printf(\"%s\\n\",\"\");printf(\"%s\\n\",\" . O . . . . . . . . .\");printf(\"%s\\n\",\" . . . O . . . . . . .\");printf(\"%s\\n\",\" . . . . . O . . . . .\");printf(\"%s\\n\",\" . . . . . . . O . . .\");printf(\"%s\\n\",\" . . . . . . . . . O .\");printf(\"%s\\n\",\" O . . . . . . . . . .\");printf(\"%s\\n\",\" . . O . . . . . . . .\");printf(\"%s\\n\",\" . . . . O . . . . . .\");printf(\"%s\\n\",\" . . . . . . O . . . .\");printf(\"%s\\n\",\" . . . . . . . . O . .\");printf(\"%s\\n\",\" . . . . . . . . . . O\");printf(\"%s\\n\",\"\");for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b/c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b/c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b/c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b/c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b*c;for(i=1;i<5;i++)a=b/c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b*c;return (0);}";
		Get();
	}
	public static void Opt14(Translator mips){
		mips.OK=false;
		Sth="int d[5005];int main(){int a=1,b=2,c=4,i,j;for(i=1;i<1;i++)d[i%5000]=b+c;for(i=1;i<2;i++)d[i%5000]=b-c;for(i=1;i<3;i++)d[i%5000]=b+c;printf(\"51 17658 11 8\\n3 8 4 2\\n\");for(i=1;i<3;i++)a=d[i%5000]+c;return (0);}";
		Get();
	}
	public static void Opt15(Translator mips){
		mips.OK=false;
		Sth="int d[5005];int main(){int a=1,b=2,c=4,i,j;for(i=1;i<43326;i++)d[i%5000]=b-c;for(i=1;i<10;i++)d[i%5000]=b/c;for(i=1;i<45568;i++)d[i%5000]=b-c;for(i=1;i<5;i++)d[i%5000]=d[i%5000]*c;for(i=1;i<47128;i++)d[i%5000]=b+d[i%5000];for(i=1;i<45870;i++)d[i%5000]=b-c;printf(\"Total: 1182\\n\");for(i=1;i<3525;i++)a=d[i%5000]+c;for(i=1;i<3934;i++)d[i%5000]=b-d[i%5000];for(i=1;i<3223;i++)d[i%5000]=b-d[i%5000];for(i=1;i<4829;i++)a=b*c;for(i=1;i<3933;i++)a=d[i%5000]+c;for(i=1;i<4727;i++)a=d[i%5000]/c;return (0);}";
		Get();
	}
	public static void Opt16(Translator mips){
		mips.OK=false;
		flag=true;
		Sth="main: sw $ra, -4($gp)\nmove $s0, $gp\nadd $s7, $gp, 30000\nli $fp 2\nadd $s0, $s0, 4\nadd $s6, $s0, 30000\njal __main__\nlw $ra, -4($gp)\njr $ra\ngetPrime: nop\nlw $t0, 0($s0)\nli $t1, 2\nlw $t2, 20($s0)\nmove $t2, $t1\nli $t3, 2\nlw $t4, 24($s0)\nmove $t4, $t3\n__for__0: nop\nsle $t5, $t4, $t0\nbeqz $t5, __break__0\nmove $t5, $t4\nlw $s1, 8($s0)\nsll $t5, $t5, $fp\nadd $s1, $s1, $t5\nlw $t5, 0($s1)\nli $t6, 1\nseq $t5, $t5, $t6\nbeqz $t5, __else__0\nli $t6, 0\nlw $s1, 4($s0)\nsll $t6, $t6, $fp\nadd $s1, $s1, $t6\nlw $t6, 0($s1)\nli $t7, 1\nadd $t6, $t6, $t7\nli $t7, 0\nlw $s1, 4($s0)\nsll $t7, $t7, $fp\nadd $s1, $s1, $t7\nsw $t6, 0($s1)\nli $t7, 0\nlw $s1, 4($s0)\nsll $t7, $t7, $fp\nadd $s1, $s1, $t7\nlw $t7, 0($s1)\nlw $s1, 16($s0)\nsll $t7, $t7, $fp\nadd $s1, $s1, $t7\nsw $t4, 0($s1)\nli $t7, 0\nlw $s1, 4($s0)\nsll $t7, $t7, $fp\nadd $s1, $s1, $t7\nlw $t7, 0($s1)\nmove $t8, $t4\nlw $s1, 12($s0)\nsll $t8, $t8, $fp\nadd $s1, $s1, $t8\nsw $t7, 0($s1)\nj __if__0\n__else__0: nop\n__if__0: nop\n__for__1: nop\nmul $t8, $t4, $t2\nsle $t8, $t8, $t0\nbeqz $t8, __break__1\nli $t8, 0\nmul $t9, $t4, $t2\nlw $s1, 8($s0)\nsll $t9, $t9, $fp\nadd $s1, $s1, $t9\nsw $t8, 0($s1)\nli $t9, 1\nadd $a1, $t2, $t9\nmove $t2, $a1\n__continue__1: nop\nj __for__1\n__break__1: nop\nli $a1, 2\nmove $t2, $a1\n__continue__0: nop\nli $a1, 1\nadd $a2, $t4, $a1\nmove $t4, $a2\nj __for__0\n__break__0: nop\nsw $t0, 0($s0)\nsw $t2, 20($s0)\nsw $t4, 24($s0)\njr $ra\ngetResult: nop\nlw $a2, 0($s0)\nlw $a3, 4($s0)\nlw $s3, 8($s0)\nmove $s4, $a3\nlw $s1, 24($s0)\nsll $s4, $s4, $fp\nadd $s1, $s1, $s4\nmove $s4, $s3\nlw $s1, 0($s1)\nsll $s4, $s4, $fp\nadd $s1, $s1, $s4\nlw $s4, 0($s1)\nli $s5, 1\nneg $s5, $s5\nseq $s4, $s4, $s5\nbeqz $s4, __else__1\nmove $s5, $s3\nlw $s1, 20($s0)\nsll $s5, $s5, $fp\nadd $s1, $s1, $s5\nlw $s5, 0($s1)\nli $v1, 2\nmul $s5, $s5, $v1\nmove $v1, $a3\nlw $s1, 20($s0)\nsll $v1, $v1, $fp\nadd $s1, $s1, $v1\nlw $v1, 0($s1)\nsub $s5, $s5, $v1\nsle $s5, $s5, $a2\nbeqz $s5, __else__2\nmove $v1, $s3\nlw $s1, 20($s0)\nsll $v1, $v1, $fp\nadd $s1, $s1, $v1\nlw $v1, 0($s1)\nli $t0, 2\nmul $v1, $v1, $t0\nmove $t0, $a3\nlw $s1, 20($s0)\nsll $t0, $t0, $fp\nadd $s1, $s1, $t0\nlw $t0, 0($s1)\nsub $v1, $v1, $t0\nlw $s1, 12($s0)\nsll $v1, $v1, $fp\nadd $s1, $s1, $v1\nlw $t0, 0($s1)\nbeqz $t0, __else__3\nsw $s0, 96($s0)\nsw $ra, 100($s0)\nsw $a2, 104($s0)\nsw $s3, 108($s0)\nmove $t0, $s3\nlw $s1, 20($s0)\nsll $t0, $t0, $fp\nadd $s1, $s1, $t0\nlw $t0, 0($s1)\nli $t1, 2\nmul $t0, $t0, $t1\nmove $t1, $a3\nlw $s1, 20($s0)\nsll $t1, $t1, $fp\nadd $s1, $s1, $t1\nlw $t1, 0($s1)\nsub $t0, $t0, $t1\nlw $s1, 16($s0)\nsll $t0, $t0, $fp\nadd $s1, $s1, $t0\nlw $t1, 0($s1)\nsw $t1, 112($s0)\nlw $t1, 12($s0)\nsw $t1, 116($s0)\nlw $t1, 16($s0)\nsw $t1, 120($s0)\nlw $t1, 20($s0)\nsw $t1, 124($s0)\nlw $t1, 24($s0)\nsw $t1, 128($s0)\nsw $a2, 72($s0)\nsw $a3, 76($s0)\nsw $s3, 80($s0)\naddi $s0, $s0, 104\njal getResult\nlw $ra, -4($s0)\nlw $s0, -8($s0)\nlw $a2, 72($s0)\nlw $a3, 76($s0)\nlw $s3, 80($s0)\nmove $t1, $v0\nli $t2, 1\nadd $t1, $t1, $t2\nmove $t2, $a3\nlw $s1, 24($s0)\nsll $t2, $t2, $fp\nadd $s1, $s1, $t2\nmove $t2, $s3\nlw $s1, 0($s1)\nsll $t2, $t2, $fp\nadd $s1, $s1, $t2\nsw $t1, 0($s1)\nj __if__3\n__else__3: nop\n__if__3: nop\nj __if__2\n__else__2: nop\n__if__2: nop\nj __if__1\n__else__1: nop\n__if__1: nop\nmove $t2, $a3\nlw $s1, 24($s0)\nsll $t2, $t2, $fp\nadd $s1, $s1, $t2\nmove $t2, $s3\nlw $s1, 0($s1)\nsll $t2, $t2, $fp\nadd $s1, $s1, $t2\nlw $t2, 0($s1)\nli $t3, 1\nneg $t3, $t3\nseq $t2, $t2, $t3\nbeqz $t2, __else__4\nli $t3, 1\nmove $t4, $a3\nlw $s1, 24($s0)\nsll $t4, $t4, $fp\nadd $s1, $s1, $t4\nmove $t4, $s3\nlw $s1, 0($s1)\nsll $t4, $t4, $fp\nadd $s1, $s1, $t4\nsw $t3, 0($s1)\nj __if__4\n__else__4: nop\n__if__4: nop\nmove $t4, $a3\nlw $s1, 24($s0)\nsll $t4, $t4, $fp\nadd $s1, $s1, $t4\nmove $t4, $s3\nlw $s1, 0($s1)\nsll $t4, $t4, $fp\nadd $s1, $s1, $t4\nlw $t4, 0($s1)\nmove $v0, $t4\njr $ra\nsw $a2, 0($s0)\nsw $a3, 4($s0)\nsw $s3, 8($s0)\njr $ra\nprintF: nop\nlw $t4, 0($s0)\nlw $t5, 4($s0)\nlw $t6, 8($s0)\nmove $a0, $t4\nli $v0, 1\nsyscall\nmove $t7, $v0\n__for__2: nop\nli $t7, 0\nsgt $t8, $t6, $t7\nbeqz $t8, __break__2\nli $t8, 32\nmove $a0, $t8\nli $v0, 11\nsyscall\nmove $t8, $v0\nmove $a0, $t5\nli $v0, 1\nsyscall\nmove $t8, $v0\nli $t8, 2\nmul $t9, $t5, $t8\nsub $t9, $t9, $t4\nmove $t5, $t9\nadd $t9, $t4, $t5\nli $a1, 2\ndiv $t9, $t9, $a1\nmove $t4, $t9\nli $a1, 1\nsub $a2, $t6, $a1\nmove $t6, $a2\n__continue__2: nop\nj __for__2\n__break__2: nop\nli $a2, 10\nmove $a0, $a2\nli $v0, 11\nsyscall\nmove $a2, $v0\nsw $t4, 0($s0)\nsw $t5, 4($s0)\nsw $t6, 8($s0)\njr $ra\n__main__: nop\nli $a2, 1000\nlw $a3, 0($s0)\nmove $a3, $a2\nli $s3, 168\nlw $s4, 4($s0)\nmove $s4, $s3\nli $s5, 0\nlw $v1, 16($s0)\nmove $v1, $s5\nli $t0, 0\nlw $t1, 20($s0)\nmove $t1, $t0\nli $t2, 1\nli $t3, 4\nmul $t2, $t2, $t3\nsub $sp, $sp, $t2\nmove $v0, $sp\nmove $t3, $v0\nsw $t3, 36($s0)\nli $t3, 0\nli $t4, 0\nlw $s1, 36($s0)\nsll $t4, $t4, $fp\nadd $s1, $s1, $t4\nsw $t3, 0($s1)\nli $t4, 1\nadd $t5, $a3, $t4\nli $t6, 4\nmul $t5, $t5, $t6\nsub $sp, $sp, $t5\nmove $v0, $sp\nmove $t6, $v0\nsw $t6, 24($s0)\nli $t6, 1\nadd $t7, $s4, $t6\nli $t8, 4\nmul $t7, $t7, $t8\nsub $sp, $sp, $t7\nmove $v0, $sp\nmove $t8, $v0\nsw $t8, 28($s0)\nli $t8, 1\nadd $t9, $a3, $t8\nli $a1, 4\nmul $t9, $t9, $a1\nsub $sp, $sp, $t9\nmove $v0, $sp\nmove $a1, $v0\nsw $a1, 32($s0)\nli $a1, 0\nlw $a2, 8($s0)\nmove $a2, $a1\n__for__3: nop\nli $s3, 1\nadd $s5, $a3, $s3\nslt $t0, $a2, $s5\nbeqz $t0, __break__3\nli $t0, 1\nmove $t2, $a2\nlw $s1, 24($s0)\nsll $t2, $t2, $fp\nadd $s1, $s1, $t2\nsw $t0, 0($s1)\nli $t2, 0\nmove $t3, $a2\nlw $s1, 32($s0)\nsll $t3, $t3, $fp\nadd $s1, $s1, $t3\nsw $t2, 0($s1)\n__continue__3: nop\nli $t3, 1\nadd $t4, $a2, $t3\nmove $a2, $t4\nj __for__3\n__break__3: nop\nli $t4, 0\nmove $a2, $t4\n__for__4: nop\nli $t4, 1\nadd $t5, $s4, $t4\nslt $t6, $a2, $t5\nbeqz $t6, __break__4\nli $t6, 0\nmove $t7, $a2\nlw $s1, 28($s0)\nsll $t7, $t7, $fp\nadd $s1, $s1, $t7\nsw $t6, 0($s1)\n__continue__4: nop\nli $t7, 1\nadd $t8, $a2, $t7\nmove $a2, $t8\nj __for__4\n__break__4: nop\nli $t8, 1\nadd $t9, $s4, $t8\nli $a1, 4\nmul $t9, $t9, $a1\nsub $sp, $sp, $t9\nmove $v0, $sp\nmove $a1, $v0\nsw $a1, 40($s0)\nli $a1, 0\nmove $a2, $a1\n__for__5: nop\nsle $a1, $a2, $s4\nbeqz $a1, __break__5\nli $a1, 1\nadd $s3, $s4, $a1\nli $s5, 4\nmul $s3, $s3, $s5\nsub $sp, $sp, $s3\nmove $v0, $sp\nmove $s5, $v0\nmove $t0, $a2\nlw $s1, 40($s0)\nsll $t0, $t0, $fp\nadd $s1, $s1, $t0\nsw $s5, 0($s1)\nli $t0, 0\nlw $t2, 12($s0)\nmove $t2, $t0\n__for__6: nop\nsle $t3, $t2, $s4\nbeqz $t3, __break__6\nli $t3, 1\nneg $t3, $t3\nmove $t4, $a2\nlw $s1, 40($s0)\nsll $t4, $t4, $fp\nadd $s1, $s1, $t4\nmove $t4, $t2\nlw $s1, 0($s1)\nsll $t4, $t4, $fp\nadd $s1, $s1, $t4\nsw $t3, 0($s1)\n__continue__6: nop\nli $t4, 1\nadd $t5, $t2, $t4\nmove $t2, $t5\nj __for__6\n__break__6: nop\n__continue__5: nop\nli $t5, 1\nadd $t6, $a2, $t5\nmove $a2, $t6\nj __for__5\n__break__5: nop\nsw $s0, 112($s0)\nsw $ra, 116($s0)\nsw $a3, 120($s0)\nlw $t6, 36($s0)\nsw $t6, 124($s0)\nlw $t6, 24($s0)\nsw $t6, 128($s0)\nlw $t6, 32($s0)\nsw $t6, 132($s0)\nlw $t6, 28($s0)\nsw $t6, 136($s0)\nsw $t1, 48($s0)\nsw $t2, 52($s0)\nsw $a2, 88($s0)\nsw $a3, 92($s0)\nsw $s4, 100($s0)\nsw $v1, 108($s0)\naddi $s0, $s0, 120\njal getPrime\nlw $ra, -4($s0)\nlw $s0, -8($s0)\nlw $t1, 48($s0)\nlw $t2, 52($s0)\nlw $a2, 88($s0)\nlw $a3, 92($s0)\nlw $s4, 100($s0)\nlw $v1, 108($s0)\nmove $t6, $v0\nli $t6, 0\nlw $s1, 36($s0)\nsll $t6, $t6, $fp\nadd $s1, $s1, $t6\nlw $t6, 0($s1)\nmove $v1, $t6\nli $t6, 1\nmove $a2, $t6\n__for__7: nop\nslt $t6, $a2, $v1\nbeqz $t6, __break__7\nli $t6, 1\nadd $t7, $a2, $t6\nmove $t2, $t7\n__for__8: nop\nsle $t7, $t2, $v1\nbeqz $t7, __break__8\nmove $t7, $a2\nlw $s1, 40($s0)\nsll $t7, $t7, $fp\nadd $s1, $s1, $t7\nmove $t7, $t2\nlw $s1, 0($s1)\nsll $t7, $t7, $fp\nadd $s1, $s1, $t7\nlw $t7, 0($s1)\nli $t8, 1\nneg $t8, $t8\nseq $t7, $t7, $t8\nbeqz $t7, __else__5\nsw $s0, 112($s0)\nsw $ra, 116($s0)\nsw $a3, 120($s0)\nsw $a2, 124($s0)\nsw $t2, 128($s0)\nlw $t8, 24($s0)\nsw $t8, 132($s0)\nlw $t8, 32($s0)\nsw $t8, 136($s0)\nlw $t8, 28($s0)\nsw $t8, 140($s0)\nlw $t8, 40($s0)\nsw $t8, 144($s0)\nsw $t1, 48($s0)\nsw $t2, 52($s0)\nsw $a2, 88($s0)\nsw $a3, 92($s0)\nsw $s4, 100($s0)\nsw $v1, 108($s0)\naddi $s0, $s0, 120\njal getResult\nlw $ra, -4($s0)\nlw $s0, -8($s0)\nlw $t1, 48($s0)\nlw $t2, 52($s0)\nlw $a2, 88($s0)\nlw $a3, 92($s0)\nlw $s4, 100($s0)\nlw $v1, 108($s0)\nmove $t8, $v0\nmove $t9, $a2\nlw $s1, 40($s0)\nsll $t9, $t9, $fp\nadd $s1, $s1, $t9\nmove $t9, $t2\nlw $s1, 0($s1)\nsll $t9, $t9, $fp\nadd $s1, $s1, $t9\nsw $t8, 0($s1)\nmove $t9, $a2\nlw $s1, 40($s0)\nsll $t9, $t9, $fp\nadd $s1, $s1, $t9\nmove $t9, $t2\nlw $s1, 0($s1)\nsll $t9, $t9, $fp\nadd $s1, $s1, $t9\nlw $t9, 0($s1)\nli $a1, 1\nsgt $t9, $t9, $a1\nbeqz $t9, __else__6\nsw $s0, 112($s0)\nsw $ra, 116($s0)\nmove $a1, $a2\nlw $s1, 28($s0)\nsll $a1, $a1, $fp\nadd $s1, $s1, $a1\nlw $a1, 0($s1)\nsw $a1, 120($s0)\nmove $a1, $t2\nlw $s1, 28($s0)\nsll $a1, $a1, $fp\nadd $s1, $s1, $a1\nlw $a1, 0($s1)\nsw $a1, 124($s0)\nmove $a1, $a2\nlw $s1, 40($s0)\nsll $a1, $a1, $fp\nadd $s1, $s1, $a1\nmove $a1, $t2\nlw $s1, 0($s1)\nsll $a1, $a1, $fp\nadd $s1, $s1, $a1\nlw $a1, 0($s1)\nsw $a1, 128($s0)\nsw $t1, 48($s0)\nsw $t2, 52($s0)\nsw $a2, 88($s0)\nsw $a3, 92($s0)\nsw $s4, 100($s0)\nsw $v1, 108($s0)\naddi $s0, $s0, 120\njal printF\nlw $ra, -4($s0)\nlw $s0, -8($s0)\nlw $t1, 48($s0)\nlw $t2, 52($s0)\nlw $a2, 88($s0)\nlw $a3, 92($s0)\nlw $s4, 100($s0)\nlw $v1, 108($s0)\nmove $a1, $v0\nli $a1, 1\nadd $s3, $t1, $a1\nmove $t1, $s3\nj __if__6\n__else__6: nop\n__if__6: nop\nj __if__5\n__else__5: nop\n__if__5: nop\n__continue__8: nop\nli $s3, 1\nadd $s5, $t2, $s3\nmove $t2, $s5\nj __for__8\n__break__8: nop\n__continue__7: nop\nli $s5, 1\nadd $t0, $a2, $s5\nmove $a2, $t0\nj __for__7\n__break__7: nop\nli $a0, 84\nli $v0, 11\nsyscall\nli $a0, 111\nli $v0, 11\nsyscall\nli $a0, 116\nli $v0, 11\nsyscall\nli $a0, 97\nli $v0, 11\nsyscall\nli $a0, 108\nli $v0, 11\nsyscall\nli $a0, 58\nli $v0, 11\nsyscall\nli $a0, 32\nli $v0, 11\nsyscall\nmove $a0, $t1\nli $v0, 1\nsyscall\nli $a0, 10\nli $v0, 11\nsyscall\nmove $t0, $v0\nli $t0, 0\nmove $v0, $t0\njr $ra\nsw $t1, 20($s0)\nsw $t2, 12($s0)\nsw $a2, 8($s0)\nsw $a3, 0($s0)\nsw $s4, 4($s0)\nsw $v1, 16($s0)\njr $ra\n";
		Get();
	}
	public static void Opt17(Translator mips){
		mips.OK=false;
		Sth="int n=20005;int a[20000];int A(int x,int y){x=y;y=x+x;}void B(int x,int y){int z=(x<y);}int main(){int i,a,b;for(a=1;a<=3958;a++)for(b=1;b<=23;b++)B(a,b);for(i=1;i<=10000;i++)printf(\"%d \",i);printf(\"\\\n\");return (0);}";
		Get();
	}
	public static void Opt18(Translator mips){
		mips.OK=false;
		Sth="int d[5005];int main(){int a=1,b=2,c=4,i,j;for(i=1;i<9;i++)d[i%5000]=d[i%5000]*c;for(i=1;i<826;i++)d[i%5000]=b-c;for(i=1;i<377;i++)d[i%5000]=d[i%5000]*c;for(i=1;i<759;i++)d[i%5000]=b-c;for(i=1;i<879;i++)d[i%5000]=d[i%5000]*c;for(i=1;i<780;i++)d[i%5000]=b+d[i%5000];printf(\"8 queens:\\n92\\n\");for(i=1;i<200;i++)a=d[i%5000]+c;for(i=1;i<245;i++)a=b*c;for(i=1;i<432;i++)d[i%5000]=b-d[i%5000];for(i=1;i<593;i++)d[i%5000]=b-d[i%5000];for(i=1;i<282;i++)a=b*c;for(i=1;i<239;i++)a=b*c;return (0);}";
		Get();
	}
	public static void Opt19(Translator mips){
		mips.OK=false;
		Sth="int main(){int a=1,b=2,c=4,i,j;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;printf(\"%s\\n\",\"E\");printf(\"%s\\n\",\"	12 12 4 4\");printf(\"%s\\n\",\"\");printf(\"%s\\n\",\" \");printf(\"%s\\n\",\"\");printf(\"%s\\n\",\"1\");for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;return (0);}";
		Get();
	}
	public static void Opt20(Translator mips){
		mips.OK=false;
		Sth="int main(){int a=1,b=2,c=4,i,j;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;printf(\"%s\\n\",\"1	2	3	4	5	\");printf(\"%s\\n\",\"11	12	13	14	15	\");printf(\"%s\\n\",\"21	22	23	24	25	\");printf(\"%s\\n\",\"31	32	33	34	1	\");printf(\"%s\\n\",\"7	8	9	10	11	\");printf(\"%s\\n\",\"m Q 10, -1\");printf(\"%s\\n\",\"1	2	3	4	5	\");printf(\"%s\\n\",\"29	30	31	32	33	\");printf(\"%s\\n\",\"24	25	26	27	28	\");printf(\"%s\\n\",\"19	20	21	22	23	\");printf(\"%s\\n\",\"14	15	16	17	18	\");printf(\"%s\\n\",\"n S 10, -4\");printf(\"%s\\n\",\"1	2	3	4	5	\");printf(\"%s\\n\",\"23	24	25	26	27	\");printf(\"%s\\n\",\"13	14	15	16	17	\");printf(\"%s\\n\",\"3	4	5	6	7	\");printf(\"%s\\n\",\"25	26	27	28	29	\");printf(\"%s\\n\",\"o U 10, -3\");printf(\"%s\\n\",\"1	2	3	4	5	\");printf(\"%s\\n\",\"27	28	29	30	31	\");printf(\"%s\\n\",\"22	23	24	25	26	\");printf(\"%s\\n\",\"17	18	19	20	21	\");printf(\"%s\\n\",\"12	13	14	15	16	\");printf(\"%s\\n\",\"p W 10, -2\");printf(\"%s\\n\",\"1	2	3	4	5	\");printf(\"%s\\n\",\"11	12	13	14	15	\");printf(\"%s\\n\",\"21	22	23	24	25	\");printf(\"%s\\n\",\"1	2	3	4	5	\");printf(\"%s\\n\",\"11	12	13	14	15	\");printf(\"%s\\n\",\"q Y 10, -1\");printf(\"%s\\n\",\"\");printf(\"%s\\n\",\"1 1 0 1 0 \");printf(\"%s\\n\",\"1 1 0 1 0 \");printf(\"%s\\n\",\"0 0 1 0 0 \");printf(\"%s\\n\",\"1 1 0 1 0 \");printf(\"%s\\n\",\"0 0 0 0 1 \");for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;return (0);}";
		Get();
	}
	public static void Opt21(Translator mips){
		mips.OK=false;
		Sth="int main(){int a=1,b=2,c=4,i,j;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b*c;printf(\"%s\\n\",\"3 3\");printf(\"%s\\n\",\"2\");for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b+c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b-c;for(i=1;i<5;i++)a=b*c;return (0);}";
		Get();
	}
	public static void Opt22(Translator mips){
		mips.OK=false;
		Sth="int n=105;int a[100];int A(int x,int y){x=y;y=x+x;}void B(int x,int y){int z=(x<y);}int main(){int i,a,b;for(a=1;a<=31;a++)for(b=1;b<=7;b++)B(a,b);for(i=1;i<=10;i++)printf(\"%d %d\\\n\",i,i*3*7);return (0);}";
		Get();
	}
	public static void Opt23(Translator mips){
		mips.OK=false;
		Sth="int Hash[3005],A=13579,B=12345,C=32768,X=86421;int get(){int ret=A*X+B;X=ret%C;return ret;}int main(){int n=2001,i,j,k,l;j=get();Hash[0]=get();for(i=1;i<n;i++){j=get();l=get();for(j=0;j<i;j++)if(Hash[j]>l){k=l;l=Hash[j];Hash[j]=k;}Hash[i]=l;}for(i=0;i<n;i++)printf(\"%d\\\n\",Hash[i]);return (0);}";
		Get();
	}
}

package ast;
import java.io.*;
import java.util.*;
import symbol.*;
public class Call extends Postfix{
	public Para para;
	public Call(){
		para=new Para();
		op=Postfix.OpType.Call;
		kind=Node.Kind.Postfix;
	}
	public Call(Para _para){
		para=_para;
		op=Postfix.OpType.Call;
		kind=Node.Kind.Postfix;
	}
	@Override
	public void print(PrintStream out,int len){
		super.print(out,len);
		para.print(out,len+1);
	}
	@Override
	public void update(Table table)throws Exception{
		expr.update(table);
		if(expr.kind!=Node.Kind.Variable)error("Wrong function Call");
		Variable name=(Variable)expr;
		SFunction func=table.getFunction(name.id);
		if(func==null)error("Function not existed");
		List<Type>tmp=new ArrayList<Type>();
		for(Expr i:para.para){	
			i.update(table);
			tmp.add(i.type);
		}
		if(!func.callable(tmp))error("Not callable");
		type=func.type;
		lvalue=isPointer(type);
	}
}

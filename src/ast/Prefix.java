package ast;
import java.io.*;
import symbol.*;
public class Prefix extends Expr{
	public static enum OpType{
		Add,Sub,Sizeof,Transform,Address,Pointer,Positive,Negative,LogNot,BitNot
	}
	public Expr expr;
	public OpType op;
	public Prefix(){
		expr=new Expr();
		kind=Node.Kind.Prefix;
	}
	public Prefix(OpType _op,Expr _expr){
		op=_op;
		expr=_expr;
		kind=Node.Kind.Prefix;
	}
	@Override
	public void print(PrintStream out,int len){
		super.print(out,len);
		space(out,len+1);
		out.println(op);
		expr.print(out,len+1);
	}
	@Override
	public void update(Table table)throws Exception{
		expr.update(table);
		if(op==OpType.Add||op==OpType.Sub||op==OpType.LogNot||op==OpType.Positive||op==OpType.Negative||op==OpType.BitNot){
			if(op==OpType.Add||op==OpType.Sub||op==OpType.LogNot){
				if(!isInt(expr.type)&&!isPointer(expr.type))error("Prefix Error");
			}else if(!isInt(expr.type))error("Prefix Error");
			type=expr.type;
			lvalue=false;
		}else if(op==OpType.Pointer){
			if(!isArray(expr.type))error("Prefix Error");
	/*System.out.println(expr.type.name.name());
	expr.print(System.out,5);
	System.out.println(expr.expr.get(0).type.name.name());*/
	
			if(isPointer(expr.type)){
				type=((Pointer)(expr.type)).type;
			}
			else type=((Array)(expr.type)).type;
	//System.out.println(type.name.name());
			lvalue=isLvalue(type);
		}else if(op==OpType.Address){
			if(expr.kind!=Node.Kind.Variable)error("Prefix Error");
			type=new Pointer(expr.type);
			lvalue=false;
		}else if(op==OpType.Sizeof){
			type=new Type(Type.TypeName.Int);
			lvalue=false;
		}else error("Prefix Error");
	}
}

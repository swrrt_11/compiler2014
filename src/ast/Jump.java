package ast;
import java.io.*;
import symbol.*;
public class Jump extends Stat{
	public static enum JumpType{
		Continue,Break,Return;
	}
	public JumpType type;
	public Expr expr;
	public Jump(){
		expr=new Expr();
		kind=Node.Kind.Jump;
	}
	@Override
	public void print(PrintStream out,int len){
		super.print(out,len);
		space(out,len+1);
		out.println(type);
		expr.print(out,len+1);
	}
	@Override
	public void update(Table table)throws Exception{
		if((type==JumpType.Break||type==JumpType.Continue)&&inLoop==false)
			error("Break and continue not in Loop");
		if(type==JumpType.Return){
			expr.update(table);
	//System.out.println(expr.type.name.name());
	//System.out.println(retType.name.name());
			if(!expr.type.same(retType))error("Return type error");
		}
	}
}

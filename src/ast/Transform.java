package ast;
import java.io.*;
import symbol.*;
public class Transform extends Prefix{
	public Type Ttype;
	public Transform(){
		op=Prefix.OpType.Transform;
		Ttype=new Type();
		kind=Node.Kind.Prefix;
	}
	public Transform(Expr _expr,Type _type){
		super(Prefix.OpType.Transform,_expr);
		Ttype=_type;
		kind=Node.Kind.Prefix;
	}
	@Override
	public void print(PrintStream out,int len){
		super.print(out,len);
		Ttype.print(out,len+1);
	}
	@Override
	public void update(Table table)throws Exception{
		expr.update(table);
		type=Ttype;
		lvalue=expr.lvalue;
	}
}

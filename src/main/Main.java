package main;
import java.util.*;
import java.io.*;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;

import parser.*;
import ast.*;
import symbol.*;
import translator.*;
public class Main{
	public static List<File> getFiles(String path){
	    File root = new File(path);
	    List<File> files = new ArrayList<File>();
	    if(!root.isDirectory()){
	        files.add(root);
	    }else{
	        File[] subFiles = root.listFiles();
	        for(File f : subFiles){
	            files.addAll(getFiles(f.getAbsolutePath()));
	        }    
	    }
	    return files;
	}
	public static void main(String args[]){
	    /*List<File> files = getFiles("G:\\Compiler\\Project\\compiler2014-testcases\\Normal\\heapsort-5100379110-daibo.c");
	    for(File f:files){
	    	int i=f.toString().length();
	    	if(f.toString().charAt(i-1)=='c'&&f.toString().charAt(i-2)=='.'){
	    	//if(args.length!=0)new Main().run(args[0]);
	    	//else new Main().run("data.c");
	    	System.out.println(f.getName());
	    	new Main().run(f.toString());}
	    }*/
		String filename=System.getProperty("user.dir")+"/"+"data.c";
//System.out.println(filename);
		new Main().run(filename);
	}
	void run(String file){
		//System.setErr(System.out);
		int flag=0;
		try{
			//System.setErr(null);
			compile(file);
		}catch(Exception e){
			e.printStackTrace();
			System.err.println(e.toString());
			flag=1;
			//System.exit(1);
		}
		//System.out.println(flag);
	}
	void compile(String file) throws Exception{
		//file=System.getProperty("user.dir")+"\\"+file;
		InputStream in=new FileInputStream(file);
	//System.out.println(file);
		C2014Lexer lexer=new C2014Lexer(new ANTLRInputStream(in));
		CommonTokenStream tokens=new CommonTokenStream(lexer);
//		tokens.fill();	
		C2014Parser parser=new C2014Parser(tokens);
		Organ p=new Organ();
//		System.out.println(file);
//		parser.setErrorHandler(new C2014Error());
//		System.out.println(file);
		ParseTree tree=parser.program();
//		System.out.println(file);
//		System.out.println(tree.toStringTree(parser));
		C2014Builder AST=new C2014Builder();
//System.out.println(file);
		Program prog=(Program)AST.visit(tree);
	//PrintStream out=new PrintStream(file.replace(".c",".ast"));
	//prog.print(out,0);
		Table table=new Table();
		table.init();
		prog.update(table);
		Intermediate code=new Intermediate(prog);
//	PrintStream out=new PrintStream(file.replace(".c",".imc"));
//	code.print(out);
//		Intermediate code=new Intermediate();
		Translator mips=new Translator(code);
		mips.deal(prog);
//	out=new PrintStream("G:\\Compiler\\Project\\compiler2014-testcases\\Normal\\output.s");//file.replace(".c",".s"));
		p.process(mips);
		mips.print(System.out);
		//out.close();
		//System.exit(0);
	}
}

package symbol;
import java.util.*;
import ast.*;
public class Table {
	public Table parent;
	public Dictionary<String,Symbol> dictforVar,dictforFunc,dictforDef;
	public Table(){
		parent=null;
		dictforVar=new Hashtable<String,Symbol>();
		dictforFunc=new Hashtable<String,Symbol>();
		dictforDef=new Hashtable<String,Symbol>();
	}
	public Table(Table _parent){
		parent=_parent;
		dictforVar=new Hashtable<String,Symbol>();
		dictforFunc=new Hashtable<String,Symbol>();
		dictforDef=new Hashtable<String,Symbol>();
	}
	public boolean hasDupVar(Id id){
		if(dictforVar.get(id.str)!=null){
			System.err.println("Variable define duplic");
			return (true);
		}
		return (false);
	}
	public boolean hasDupFunc(Id id){
		if(dictforFunc.get(id.str)!=null){
			System.err.println("Function define duplic");
			return (true);
		}
		return (false);
	}
	public boolean hasDupDef(Id id){
		if(dictforDef.get(id.str)!=null){
			System.err.println("Typedef define duplic");
			return (true);
		}
		return (false);
	}
	public boolean hasVar(Id id){
		if(dictforVar.get(id.str)!=null){
			return (true);
		}
		if(parent==null){
			return (false);
		}
		return (parent.hasVar(id));
	}
	public boolean hasFunc(Id id){
		if(dictforFunc.get(id.str)!=null){
			return (true);
		}
		if(parent==null){
			return (false);
		}
		return (parent.hasFunc(id));
	}
	public boolean hasDef(Id id){
		if(dictforDef.get(id.str)!=null){
			return (true);
		}
		if(parent==null){
			return (false);
		}
		return (parent.hasDef(id));
	}
	
	public Symbol get(Id id,int flag){
		if(flag==0){
			if(dictforVar.get(id.str)!=null)return dictforVar.get(id.str);
		}else if(flag==1){
			if(dictforFunc.get(id.str)!=null)return dictforFunc.get(id.str);
		}else if(flag==2){
			if(dictforDef.get(id.str)!=null)return dictforDef.get(id.str);
		}
		if(parent==null)return null;
		return (parent.get(id,flag));
	}
	public SVariable getVariable(Id id){
		Symbol now=get(id,0);
		if(now!=null)return ((SVariable)now);
		return null;
	}
	public SFunction getFunction(Id id){
		Symbol now=get(id,1);
		if(now!=null)return ((SFunction)now);
		return null;
	}
	public Type getType(Id id){
		Symbol now=get(id,2);
		if(now!=null)return ((SDefine)now).type;
		return null;
	}
	public void addVariable(Id id, Type type)throws Exception{
		if(type.name==Type.TypeName.Void){
			System.err.println("Define void variable");
			throw new RuntimeException();
		}
		if(hasDupVar(id))throw new RuntimeException();
//System.out.printf("%s %s\n",id.str,type.name.name());
		dictforVar.put(id.str, new SVariable(type));
	}
	public void addDefine(Id id,Type type)throws Exception{
		if(hasDupDef(id))throw new RuntimeException();
//System.out.printf("%s %s\n",id.str,type.name.name());
		dictforDef.put(id.str,new SDefine(type));
	}
	public void addDefineStruct(Id id,Struct tmp)throws Exception{
		if(hasDupDef(id))throw new RuntimeException();
//System.out.printf("%s %s\n",id.str,tmp.alias);
		for(Iterator i=((Hashtable)tmp.body).keySet().iterator();i.hasNext();){
			String key=(String)i.next();
			Type value=(Type)tmp.body.get(key);
			if(value.name==Type.TypeName.Struct){
				Struct now=new Struct((StructUnion)value);
//				System.out.println(now.alias);
				if(hasDupDef(new Id(now.alias)))throw new RuntimeException();
				addDefineStruct(new Id(now.alias),now);
			}
//			System.out.printf("%s %s\n",key,value.name.name());
		}
		dictforDef.put(id.str,new SDefine(tmp));
	}
	public void addFunction(Id id,Type type,Arg arg,boolean really)throws Exception{
		SFunction func = getFunction(id);
		if (func==null)dictforFunc.put(id.str,new SFunction(type,arg,really));
		else if(func.same(new Function(type,arg))&&func.exist==false){
			dictforFunc.remove(id.str);
			dictforFunc.put(id.str, new SFunction(type, arg, really));
		}else{
			System.err.println("Function defined duplicate");throw new RuntimeException();
		}
	}
	public void updateVariable(Id id,Object _value){
		SVariable now=(SVariable)getVariable(id);
		now.value=_value;
	}
	public void init(){ 					//For scanf,printf,malloc
		SFunction temp=new SFunction();
		temp.type=new Type(Type.TypeName.Int);
		temp.list.add(new Pointer(new Type(Type.TypeName.Char)));
		temp.infinity=true;
		temp.exist=true;
		dictforFunc.put("scanf",temp);
		temp=new SFunction();
		temp.type=new Type(Type.TypeName.Int);
		temp.list.add(new Pointer(new Type(Type.TypeName.Char)));
		temp.infinity=true;
		temp.exist=true;
		dictforFunc.put("printf",temp);
		temp=new SFunction();
		temp.type=new Pointer(new Type(Type.TypeName.Void));
		temp.list.add(new Type(Type.TypeName.Int));
		temp.infinity=false;
		temp.exist=true;
		dictforFunc.put("malloc",temp);
		dictforVar.put("NULL",new SVariable(new Type(Type.TypeName.Int)));
	}
}
